package com.example.dewclaw_new.Handlers;

import android.content.ContentResolver;
import android.content.res.Resources;
import android.net.Uri;

import com.example.dewclaw_new.DTOs.ProfilePictureDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;

import java.io.*;

import java.io.IOException;

public class UserProfilePictureHandler {


    static FireBaseModel mModel;


    public UserProfilePictureHandler(){
    }

    public static void getUserProfilePicture(String userId, final IGlobalListener<ProfilePictureDTO> listener) {
        mModel =  (FireBaseModel) ModelManager.getInstance(EModel.FireBase);

        mModel.getUserProfilePicture(userId,new IGlobalListener<String>() {
            @Override
            public void onComplete(String object) {
                if(object != null){
                    ProfilePictureDTO localProfileDTO = new ProfilePictureDTO();
                    localProfileDTO.ExternalPicture = object;
                    localProfileDTO.NeedDefault = false;
                    listener.onComplete(localProfileDTO);
                }
                else {
                    ProfilePictureDTO localProfileDTO = new ProfilePictureDTO();
                    localProfileDTO.NeedDefault = true;
                        listener.onComplete(localProfileDTO);
                }
            }
        });
    }
}





















