package com.example.dewclaw_new.Handlers;

import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;
import com.google.firebase.auth.FirebaseAuth;

import java.security.NoSuchAlgorithmException;

public class UserDetailsHandler {

    static FireBaseModel mModel;
    static ModelManager modelManager;

    public UserDetailsHandler(){}

    public static void getUserDetails(final IGlobalListener<UserDTO> listener){
        mModel =  (FireBaseModel) ModelManager.getInstance(EModel.FireBase);
        modelManager = new ModelManager();

        FirebaseAuth fireBaseAuthInstance = mModel.getFireBaseAuthInstance();
        modelManager.getUserById(fireBaseAuthInstance.getCurrentUser().getUid(), new IGetUserListener() {
            @Override
            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    listener.onComplete(userDTO);
                }
            }
        });
    }
}
