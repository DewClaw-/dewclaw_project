package com.example.dewclaw_new.Handlers;

import com.example.dewclaw_new.DTOs.ProfilePictureDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class UserFollowingAndFollowersHandler {

    static FireBaseModel mModel;


    public UserFollowingAndFollowersHandler(){
    }

    public static void getUserFollowing(String userId, final IGlobalListener<ArrayList<UserDTO>> listener) {
        mModel =  (FireBaseModel) ModelManager.getInstance(EModel.FireBase);

        mModel.getUserFollowing(userId, new IGlobalListener<Set<UserDTO>>() {
            @Override
            public void onComplete(Set<UserDTO> users) {
                if(users != null)
                    listener.onComplete(new ArrayList<UserDTO>(users));
            }
        });
    }

    public static  void getUserFollowers(String userId, final IGlobalListener<ArrayList<UserDTO>> listener){
        mModel =  (FireBaseModel) ModelManager.getInstance(EModel.FireBase);

        mModel.getUserFollowers(userId, new IGlobalListener<Set<UserDTO>>() {
            @Override
            public void onComplete(Set<UserDTO> users) {
                listener.onComplete(new ArrayList<UserDTO>(users));
            }
        });
    }
}
