package com.example.dewclaw_new.Handlers;

import android.widget.EditText;

import com.example.dewclaw_new.DTOs.ExceptionDTO;
import com.example.dewclaw_new.Exceptions.EExceptions;
import com.example.dewclaw_new.Exceptions.ExceptionsUtil;
import com.example.dewclaw_new.Models.Intefraces.IGetExceptionListener;

public class ErrorHandler {

    static ExceptionDTO localException;

    public static void setError(final EditText view, EExceptions exception){
        ExceptionsUtil.getByKey(exception, new IGetExceptionListener() {
            @Override
            public void onComplete(ExceptionDTO exceptionDTO) {
                if(exceptionDTO != null){
                    view.setError(exceptionDTO.getText());
                    return;
                }
            }
        });
    }
}
