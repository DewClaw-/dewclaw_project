package com.example.dewclaw_new.Handlers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.Gravity;
import android.widget.Toast;

import com.example.dewclaw_new.Activities.LoginActivity;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Fragments.HomeFragment;
import com.example.dewclaw_new.Fragments.BusinessPageFragment;
import com.example.dewclaw_new.Fragments.MyDetailsFragment;
import com.example.dewclaw_new.Fragments.ProfileFragment;
import com.example.dewclaw_new.Fragments.SearchFragment;
import com.example.dewclaw_new.Fragments.UploadPostFragment;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class ChangeFragmentHandler {

    static ModelManager mModel;


    @SuppressLint("WrongConstant")
    public static void changeFragmentDisplay(Activity fromActivity, Fragment fromFragment, Integer lowerPartId, DrawerLayout drawerLayout, FragmentManager supportFragmentManager, @NonNull int menuItemId) {

        Fragment fragment = null;

        switch (menuItemId)
        {
            case R.id.home:
                fragment = new HomeFragment();
                break;
            case R.id.search:
                fragment = new SearchFragment();
                break;
            case R.id.camera:
                fragment = new UploadPostFragment();
                break;
            case R.id.likes:
                fragment = new BusinessPageFragment();
                break;
            case R.id.profile:
                fragment = new ProfileFragment();
                break;
            case R.id.details:
                fragment = new MyDetailsFragment();
                break;
            case R.id.log_out:
                mModel.getInstance(EModel.FireBase).logUserOut(); //The method will take the user to login page
                Intent intent = new Intent(MyApplication.getContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                MyApplication.getContext().startActivity(intent);
                fragment = null;
                break;
            default:
                Toast.makeText(MyApplication.getContext(),"Error",Toast.LENGTH_LONG).show();
                break;

        }

        //hide navigation drawer
        drawerLayout.closeDrawer(Gravity.START);

        if(fragment != null){
            supportFragmentManager.popBackStack();
            FragmentTransaction transaction = supportFragmentManager.beginTransaction();
            transaction.replace(lowerPartId,fragment);
            transaction.commit();
        }
    }
}
