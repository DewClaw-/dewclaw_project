package com.example.dewclaw_new.Handlers;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;

import com.example.dewclaw_new.BusinessUtils.BusinessUtils;
import com.example.dewclaw_new.Enums.EBusinessPayment;
import com.example.dewclaw_new.Enums.EBusinessType;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;

import java.util.ArrayList;
import java.util.Collections;

public class BusinessHandler {


    public static void getBusinessTypes(final Spinner view, final Activity activity){
        BusinessUtils.getAllBusinessTypes(new IGlobalListener<ArrayList<String>>() {
            @Override
            public void onComplete(ArrayList<String> typesFromDb) {
                if(typesFromDb != null){
                    Collections.sort(typesFromDb);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.support_simple_spinner_dropdown_item ,typesFromDb.toArray(new String[0]));
                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    view.setAdapter(adapter);
                }
            }
        });
    }

    public static void getBusinessPayments(final Spinner view, final Activity activity){
        BusinessUtils.getAllBusinessPayments(new IGlobalListener<ArrayList<String>>() {
            @Override
            public void onComplete(ArrayList<String> paymentsFromDb) {
                if(paymentsFromDb != null){
                    Collections.sort(paymentsFromDb);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.support_simple_spinner_dropdown_item ,paymentsFromDb.toArray(new String[0]));
                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    view.setAdapter(adapter);
                }
            }
        });
    }

    public static String getBusinessTypeEnumName(EBusinessType type){
        switch (type){
            case Training:
                return EBusinessType.Training.name();
            case Adoption:
                return EBusinessType.Adoption.name();
            case Selling:
                return EBusinessType.Selling.name();
            case DogWalking:
                return EBusinessType.DogWalking.name();
            case Barbershop:
                return EBusinessType.Barbershop.name();
            case BoardingHouse:
                return EBusinessType.BoardingHouse.name();
            case Else:
            default:
                return EBusinessType.Else.name();
        }
    }

    public static String getBusinessPaymentEnumName(EBusinessPayment payment) {
        switch (payment){
            case Free:
                return EBusinessPayment.Free.name();
            case Paid:
                return EBusinessPayment.Paid.name();
            default:
                return EBusinessPayment.Free.name();
        }
    }

    public static int getBusinessPaymentEnumValue(EBusinessPayment payment) {
        switch (payment){
            case Free:
                return 0;
            case Paid:
                return 1;
            default:
                return 0;
        }
    }

    public static int getBusinessTypeEnumValue(EBusinessType payment) {
        switch (payment){
            case Training:
                return 0;
            case Adoption:
                return 1;
            case Selling:
                return 2;
            case DogWalking:
                return 3;
            case Barbershop:
                return 4;
            case BoardingHouse:
                return 5;
            case Else:
            default:
                return 6;
        }
    }

    public static EBusinessPayment getBusinessPayment(String payment) {
        if(EBusinessPayment.Paid.name().equals(payment))
            return EBusinessPayment.Paid;
        else
            return EBusinessPayment.Free;
    }

    public static EBusinessType getBusinessType(String type) {
        if(EBusinessType.Training.name().equals(type))
            return EBusinessType.Training;
        else if(EBusinessType.Adoption.name().equals(type))
            return EBusinessType.Adoption;
        else if(EBusinessType.Selling.name().equals(type))
            return EBusinessType.Selling;
        else if(EBusinessType.DogWalking.name().equals(type))
            return EBusinessType.DogWalking;
        else if(EBusinessType.Barbershop.name().equals(type))
            return EBusinessType.Barbershop;
        else if(EBusinessType.BoardingHouse.name().equals(type))
            return EBusinessType.BoardingHouse;
        else
            return EBusinessType.Else;
    }
}
