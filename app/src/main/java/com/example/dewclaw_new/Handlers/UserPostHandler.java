package com.example.dewclaw_new.Handlers;

import com.example.dewclaw_new.DTOs.PostDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;

import java.util.Set;

public class UserPostHandler {

    static FireBaseModel mModel;


    public UserPostHandler(){
    }

    public static void getUserPosts(String userId,final IGlobalListener<Set<PostDTO>> listener){
        mModel =  (FireBaseModel) ModelManager.getInstance(EModel.FireBase);

        mModel.getUserPosts(userId, new IGlobalListener<Set<PostDTO>>() {
            @Override
            public void onComplete(Set<PostDTO> posts) {
                if(posts != null)
                    listener.onComplete(posts);
            }
        });
    }
}
