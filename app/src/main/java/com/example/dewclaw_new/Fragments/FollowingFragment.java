package com.example.dewclaw_new.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dewclaw_new.Activities.OtherProfileActivity;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FollowingFragment extends Fragment {

    RecyclerView recyclerView;

    ArrayList<UserDTO> followingList;

    FireBaseModel mModel;

    Integer currentUserId;

    public FollowingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_following, container, false);

        mModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);
        currentUserId = mModel.getFireBaseAuthInstance().getCurrentUser().hashCode();

        Bundle arguments = this.getArguments();
        followingList = arguments.getParcelableArrayList(ProfileFragment.FollowingBundleKey);
        if(followingList == null)
            followingList = new ArrayList<>();

        recyclerView = view.findViewById(R.id.following_recycler_iv);

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        recyclerView.setAdapter(new FollowingFragment.RVAdapterFollowing(followingList,getContext()));

        return view;
    }
    public class RVAdapterFollowing extends RecyclerView.Adapter<FollowingFragment.RVAdapterFollowing.FollowingViewHolder>{

        Context mContext;
        List<UserDTO> followingList;

        public RVAdapterFollowing(List<UserDTO> followers, Context context){
            followingList = followers;
            mContext = context;
        }

        @NonNull
        @Override
        public FollowingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.followers_and_following_content, parent,false);

            FollowingFragment.RVAdapterFollowing.FollowingViewHolder viewHolder = new FollowingFragment.RVAdapterFollowing.FollowingViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull FollowingViewHolder holder, int position) {
            UserDTO userDTO = followingList.get(position);

            holder.setDetails(userDTO);
        }

        @Override
        public int getItemCount() {
            if(followingList != null)
                return followingList.size();
            return 0;
        }

        public class FollowingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            UserDTO userDTO;

            View view;

            ImageView followers_and_following_content_iv;
            TextView followers_and_following_content_tv;

            public FollowingViewHolder(@NonNull View itemView) {
                super(itemView);

                view = itemView;
            }

            public void setDetails(UserDTO following){
                userDTO = following;

                followers_and_following_content_iv = view.findViewById(R.id.followers_and_following_content_iv);
                followers_and_following_content_tv = view.findViewById(R.id.followers_and_following_content_tv);

                Picasso.with(MyApplication.getContext()).load(userDTO.getProfileImage()).into(followers_and_following_content_iv);
                followers_and_following_content_tv.setText(userDTO.getFullName());

                followers_and_following_content_tv.setOnClickListener(this);
                followers_and_following_content_iv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if(userDTO.getId().equals(currentUserId)){
                    Fragment fragment = new ProfileFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.main_fragment_content,fragment);
                    transaction.commit();
                }
                else{
                    Intent intent = new Intent(MyApplication.getContext(), OtherProfileActivity.class);
                    intent.putExtra("UserId", userDTO.getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getContext().startActivity(intent);
                }
            }
        }
    }
}
