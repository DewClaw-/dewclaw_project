package com.example.dewclaw_new.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.Intefraces.IModel;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.R;
import com.firebase.ui.auth.data.model.User;

import org.w3c.dom.Text;

import java.security.NoSuchAlgorithmException;


public class MyDetailsFragment extends Fragment {

    EditText email_tv;
    EditText first_name_tv;
    EditText last_name_tv;
    EditText age_tv;
    EditText dog_name_tv;
    EditText dog_age_tv;
    EditText dog_breed_tv;

    Button update_btn;

    ImageView detail_iv;

    FireBaseModel mModel;
    ModelManager modelManager;

    public MyDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_details, container, false);

        email_tv = view.findViewById(R.id.details_email_tv);
        first_name_tv = view.findViewById(R.id.details_first_name_tv);
        last_name_tv = view.findViewById(R.id.details_last_name_tv);
        age_tv = view.findViewById(R.id.details_age_tv);
        dog_name_tv = view.findViewById(R.id.details_dog_name_tv);
        dog_age_tv = view.findViewById(R.id.details_dog_age_tv);
        dog_breed_tv = view.findViewById(R.id.details_dog_breed_tv);

        update_btn = view.findViewById(R.id.details_update_btn);

        detail_iv = view.findViewById(R.id.details_iv);

        modelManager = new ModelManager();

        init();

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserDetails();
            }
        });

        return view;
    }

    private void updateUserDetails() {
        mModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);
        int userId = mModel.getFireBaseAuthInstance().getCurrentUser().hashCode();

        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(email_tv.getText().toString());
        userDTO.setFirstName(first_name_tv.getText().toString());
        userDTO.setLastName(last_name_tv.getText().toString());
        userDTO.setAge(age_tv.getText().toString());
        userDTO.setDogName(dog_name_tv.getText().toString());
        userDTO.setDogAge(dog_age_tv.getText().toString());
        userDTO.setDogBreed(dog_breed_tv.getText().toString());


        mModel.updateUserDetails(userDTO, new IGlobalListener<UserDTO>() {
            @Override
            public void onComplete(UserDTO object) {
                if(object != null)
                    init();
            }
        });
    }

    private void init() {
        mModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);
        final String userId = mModel.getFireBaseAuthInstance().getCurrentUser().getUid();
        modelManager.getUserById(userId, new IGetUserListener() {
            @Override
            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    email_tv.setText(userDTO.getEmail());
                    first_name_tv.setText(userDTO.getFirstName());
                    last_name_tv.setText(userDTO.getLastName());
                    age_tv.setText(userDTO.getAge());
                    dog_name_tv.setText(userDTO.getDogName());
                    dog_age_tv.setText(userDTO.getDogAge());
                    dog_breed_tv.setText(userDTO.getDogBreed());

                    detail_iv.setImageResource(R.drawable.dog_and_owner);
                }
            }
        });
    }

}
