package com.example.dewclaw_new.Fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dewclaw_new.DTOs.BusinessDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.Intefraces.IModel;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import com.squareup.picasso.Picasso;

import java.security.NoSuchAlgorithmException;


public class BusinessPageFragment extends Fragment {

    LinearLayout business_no_register_a_business_linear_layout;
    LinearLayout business_already_register_a_business_linear_layout;

    TextView business_default_title;
    TextView business_very_simple_title;
    Button business_just_click_here_btn;
    ImageView business_default_image_view;

    TextView business_actual_title;
    Button business_change_details_btn;
    TextView business_type;
    TextView business_free_paid;
    TextView business_little_about_us;
    ImageView business_logo_iv;

    FireBaseModel mModel;
    ModelManager modelManager;

    String currentUserId;


    public BusinessPageFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_page, container, false);

        business_no_register_a_business_linear_layout = view.findViewById(R.id.business_no_register_a_business_linear_layout);
        business_already_register_a_business_linear_layout = view.findViewById(R.id.business_already_register_a_business_linear_layout);

        business_default_title = view.findViewById(R.id.business_default_title);
        business_very_simple_title = view.findViewById(R.id.business_very_simple_title);
        business_just_click_here_btn = view.findViewById(R.id.business_just_click_here_btn);
        business_default_image_view = view.findViewById(R.id.business_default_image_view);

        business_actual_title = view.findViewById(R.id.business_actual_title);
        business_change_details_btn = view.findViewById(R.id.business_change_details_btn);
        business_type = view.findViewById(R.id.business_type);
        business_free_paid = view.findViewById(R.id.business_free_paid);
        business_little_about_us = view.findViewById(R.id.business_little_about_us);
        business_logo_iv = view.findViewById(R.id.business_logo_iv);

        mModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);
        modelManager = new ModelManager();


        currentUserId = mModel.getFireBaseAuthInstance().getCurrentUser().getUid();

        business_just_click_here_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToNewBusinessAndUpdateBusinessDetails();
            }
        });

        business_change_details_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToNewBusinessAndUpdateBusinessDetails();
            }
        });

        business_already_register_a_business_linear_layout.setVisibility(View.GONE);
        business_no_register_a_business_linear_layout.setVisibility(View.GONE);

        modelManager.getUserById(currentUserId, new IGetUserListener() {
            @Override
            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    if(userDTO.getBusinessId() == null){
                        business_already_register_a_business_linear_layout.setVisibility(View.GONE);
                        business_no_register_a_business_linear_layout.setVisibility(View.VISIBLE);
                    }
                    else {
                        business_already_register_a_business_linear_layout.setVisibility(View.VISIBLE);
                        business_no_register_a_business_linear_layout.setVisibility(View.GONE);
                        init();
                    }
                }
            }
        });


        return view;
    }

    private void init() {
        modelManager.getUserById(currentUserId, new IGetUserListener() {
            @Override
            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    mModel.getBusinessById(userDTO.getBusinessId(), new IGlobalListener<BusinessDTO>() {
                        @Override
                        public void onComplete(BusinessDTO business) {
                            if(business != null){
                                business_actual_title.setText(""+business.getBusinessName());
                                business_type.setText(""+business.getBusinessType());
                                business_free_paid.setText(""+business.getBusinessPayment());
                                business_little_about_us.setText(""+business.getBusinessAboutUs());
                                Picasso.with(MyApplication.getContext()).load(business.getBusinessLogo()).into(business_logo_iv);
                            }
                        }
                    });
                }
            }
        });
    }

    private void moveToNewBusinessAndUpdateBusinessDetails() {
        Toast.makeText(BusinessPageFragment.this.getActivity(), "Please wait...", Toast.LENGTH_SHORT).show();
        new CountDownTimer(1000,1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                Fragment fragment = new BusinessDetailsFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putString("UserId",currentUserId);
                fragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.main_fragment_content, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        }.start();
    }

}
