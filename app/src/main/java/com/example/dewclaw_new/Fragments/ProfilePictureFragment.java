package com.example.dewclaw_new.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.dewclaw_new.Activities.MainActivity;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Generators.StringsGenerator;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import com.squareup.picasso.Picasso;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import static android.app.Activity.RESULT_OK;


public class ProfilePictureFragment extends Fragment {

    Button upload_btn;
    Button capture_btn;
    Button from_gallery_btn;
    ImageView captured_image_view;
    Button profile_rotate_btn;

    Uri mImageUri;
    Bitmap bitmap;
    final int CAPTURE_IMAGE = 1;
    final int SELECT_AN_IMAGE_FROM_GALLERY = 2;
    String mStoryTitle;
    String imageToString;

    ModelManager mModel;

    String pathFile;
    String tempImageName;

    float angle = 0;

    public ProfilePictureFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Build.VERSION.SDK_INT >= 23){
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},2);
        }



        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_picture, container, false);

        upload_btn = view.findViewById(R.id.profile_upload_btn);
        capture_btn = view.findViewById(R.id.profile_capture_btn);
        captured_image_view = view.findViewById(R.id.profile_captured_image_view);
        from_gallery_btn = view.findViewById(R.id.profile_from_gallery_btn);
        profile_rotate_btn = view.findViewById(R.id.profile_rotate_btn);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(bitmap != null){
            captured_image_view.setImageBitmap(bitmap);
        }
        capture_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    captureAnImage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        upload_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postAndImageTitle();
            }
        });

        from_gallery_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAnImageFromGellery();
            }
        });

        profile_rotate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mImageUri == null)
                    Toast.makeText(getContext(), "No picture has been chosen.", Toast.LENGTH_SHORT).show();
                else{
                    Log.d("TAG","Rotation :: Rotation ::");
                    captured_image_view.setRotation(angle);
                    angle -= 90;
                    bitmap = rotateImage(((BitmapDrawable)captured_image_view.getDrawable()).getBitmap(),angle);
                    captured_image_view.setImageBitmap(bitmap);
                    File file = new File(mImageUri.getPath());
                    if(file.exists()){
                        OutputStream os = null;
                        try {
                            os = new BufferedOutputStream(new FileOutputStream(file));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                        try {
                            os.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        mImageUri = Uri.fromFile(file);
                    }
                }
            }
        });
    }

    private void captureAnImage() throws IOException {
        Date dateNow = new Date();
        tempImageName = "image_"+dateNow.getYear()+"-"+dateNow.getMonth()+"-"+dateNow.getDay()+dateNow.getTime()+".jpg";

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File tempFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),tempImageName);
        boolean hasCreated = tempFile.createNewFile();
        boolean hasExists = tempFile.exists();
        File absoluteFile = tempFile.getAbsoluteFile();
        mImageUri = Uri.fromFile(tempFile);
        pathFile = mImageUri.getPath();
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,mImageUri);
        cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(cameraIntent, CAPTURE_IMAGE);
    }

    private void selectAnImageFromGellery(){
        Intent intent = new Intent();    //CHOOSE A PHOTO FROM GALLERY
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,SELECT_AN_IMAGE_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case CAPTURE_IMAGE:
                if(resultCode == RESULT_OK && mImageUri != null ){
                    int orientation = getResources().getConfiguration().orientation;
                    if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                        bitmap =  BitmapFactory.decodeFile(pathFile);
                    } else {
                        bitmap =  rotateImage(BitmapFactory.decodeFile(pathFile),angle);
                    }
                    if(bitmap != null){
                        Picasso.with(MyApplication.getContext()).load(mImageUri).into(captured_image_view);
                        captured_image_view.setRotation(angle);
                    }
                }
                break;
            case SELECT_AN_IMAGE_FROM_GALLERY:
                if(resultCode == RESULT_OK){
                    if(data != null){
                        mImageUri = data.getData();

                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(MyApplication.getContext().getContentResolver(), mImageUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Picasso.with(MyApplication.getContext()).load(mImageUri).into(captured_image_view);
                        captured_image_view.setRotation(angle);
                    }
                }
                break;
        }
    }

    private void postAndImageTitle() {


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setPositiveButton("Upload", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    uploadProfilePictureImage();
                } catch (NoSuchAlgorithmException e) {
                    System.out.println("Exception from MD5 algorithm.");
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    private String convertImageToString() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imageByteArray = byteArrayOutputStream.toByteArray();

        String result = Base64.encodeToString(imageByteArray,Base64.DEFAULT);

        return result;
    }

    private Bitmap rotateImage(Bitmap bitmap,float angle){
        Matrix matrix = new Matrix();
        matrix.setRotate(angle);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);
    }

    private void uploadProfilePictureImage() throws NoSuchAlgorithmException {
        final FireBaseModel fireBaseModel= ((FireBaseModel)mModel.getInstance(EModel.FireBase));

        Toast.makeText(getActivity(),"Uploading your image ...",Toast.LENGTH_LONG).show();
        MediaStore.Images.Media.insertImage(getContext().getContentResolver(), bitmap, tempImageName,"");
        fireBaseModel.uploadProfilePictureImage(generateProfileImageName(),mImageUri,"jpg", new IGlobalListener<Boolean>() {
            @Override
            public void onComplete(Boolean success) {
                if(success){
                    Toast.makeText(getActivity(),"Uploaded successfully!",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getActivity(),"Something went wrong. Please try again later.",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private String generateProfileImageName() {
        return StringsGenerator.GenerateNewUUID();
    }
}
