package com.example.dewclaw_new.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dewclaw_new.Activities.PostPageActivity;
import com.example.dewclaw_new.DTOs.PostDTO;
import com.example.dewclaw_new.DTOs.ProfilePictureDTO;
import com.example.dewclaw_new.DTOs.SearchContentDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Handlers.ChangeFragmentHandler;
import com.example.dewclaw_new.Handlers.UserFollowingAndFollowersHandler;
import com.example.dewclaw_new.Handlers.UserPostHandler;
import com.example.dewclaw_new.Handlers.UserProfilePictureHandler;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.firebase.ui.auth.data.model.User;
import com.squareup.picasso.Picasso;


public class ProfileFragment extends Fragment {


    CircleImageView profile_iv;
    TextView posts_num_tv;
    TextView followers_num_tv;
    TextView following_num_tv;

    RecyclerView recyclerView;


    Button follow_btn;

    String userId;

    public static String FollowersBundleKey = "Followers";
    public static String FollowingBundleKey = "Following";

    ArrayList<UserDTO> globalUserFollowing;
    ArrayList<UserDTO> globalUserFollowers;


    static CircleImageView static_profile_iv;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_profile, container, false);

        userId = ((FireBaseModel)ModelManager.getInstance(EModel.FireBase)).getFireBaseAuthInstance().getCurrentUser().getUid();

        profile_iv = view.findViewById(R.id.profile_image);
        follow_btn = view.findViewById(R.id.other_profile_follow_this_profile);
        posts_num_tv = view.findViewById(R.id.posts_num_tv);
        followers_num_tv = view.findViewById(R.id.followers_num_tv);
        following_num_tv = view.findViewById(R.id.following_num_tv);

        UserProfilePictureHandler.getUserProfilePicture(userId, new IGlobalListener<ProfilePictureDTO>() {
            @Override
            public void onComplete(ProfilePictureDTO object) {
                if(object != null) {
                    profile_iv = view.findViewById(R.id.profile_image);
                    if(object.NeedDefault){
                        profile_iv.setImageResource(object.DEFAULT_PROFILE_PICTURE);
                    }
                    else{
                        Picasso.with(MyApplication.getContext()).load(object.ExternalPicture).into(profile_iv);
                    }
                }
            }
        });

        UserFollowingAndFollowersHandler.getUserFollowing(userId, new IGlobalListener<ArrayList<UserDTO>>() {
            @Override
            public void onComplete(ArrayList<UserDTO> users) {
                if(users != null){
                    globalUserFollowing = users;
                    following_num_tv.setText(""+users.size());
                }
            }
        });
        UserFollowingAndFollowersHandler.getUserFollowers(userId, new IGlobalListener<ArrayList<UserDTO>>() {
            @Override
            public void onComplete(ArrayList<UserDTO> users) {
                if(users != null){
                    globalUserFollowers = users;
                    followers_num_tv.setText(""+users.size());
                }
            }
        });

        UserPostHandler.getUserPosts(userId,new IGlobalListener<Set<PostDTO>>() {
            @Override
            public void onComplete(Set<PostDTO> posts) {
                if(posts != null){
                    posts_num_tv.setText(""+posts.size());
                    recyclerView = view.findViewById(R.id.post_recycler_iv);
                    recyclerView.setLayoutManager(new GridLayoutManager(MyApplication.getContext(),3));
                    recyclerView.setAdapter(new ProfileFragment.RVAdapterPosts(new ArrayList<PostDTO>(posts),getContext()));
                }
            }
        });

        profile_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfilePictureFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(ProfileFragment.this.getId(),fragment);
                transaction.commit();
            }
        });

        followers_num_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoveToFollowersPage();
            }
        });

        following_num_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoveToFollowingePage();
            }
        });

        return view;
    }

    private void MoveToFollowersPage() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FollowersFragment followersFragment = new FollowersFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(FollowersBundleKey,globalUserFollowers);
        followersFragment.setArguments(bundle);
        fragmentTransaction.replace(this.getId(),followersFragment);
        fragmentTransaction.commit();
    }

    private void MoveToFollowingePage() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FollowingFragment followingFragment = new FollowingFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(FollowingBundleKey,globalUserFollowing);
        followingFragment.setArguments(bundle);
        fragmentTransaction.replace(this.getId(),followingFragment);
        fragmentTransaction.commit();
    }

    public class RVAdapterPosts extends RecyclerView.Adapter<ProfileFragment.RVAdapterPosts.PostViewHolder>{

        Context mContext;
        List<PostDTO> postDTOList;

        public RVAdapterPosts(List<PostDTO> posts, Context context){
            postDTOList = posts;
            mContext = context;
        }

        @NonNull
        @Override
        public ProfileFragment.RVAdapterPosts.PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.post_content, parent,false);
            ProfileFragment.RVAdapterPosts.PostViewHolder viewHolder = new ProfileFragment.RVAdapterPosts.PostViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ProfileFragment.RVAdapterPosts.PostViewHolder holder, int position) {
            PostDTO postDTO = postDTOList.get(position);

            holder.setDetails(postDTO.getPostImage(),postDTO.getPostId());
        }

        @Override
        public int getItemCount() {
            if(postDTOList != null)
                return postDTOList.size();
            return 0;
        }

        public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            View view;
            String givenPostId;

            public PostViewHolder(@NonNull View itemView) {
                super(itemView);

                view = itemView;
            }

            public void setDetails(String imageFromStorage, final String postId){

                final ImageView image_iv = view.findViewById(R.id.post_content_iv);
                Picasso.with(MyApplication.getContext()).load(imageFromStorage).into(image_iv);
                givenPostId = postId;

                image_iv.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyApplication.getContext(),PostPageActivity.class);
                intent.putExtra("PostId",givenPostId);
                intent.putExtra("UserId", userId);
                startActivity(intent);
            }
        }
    }
}






















