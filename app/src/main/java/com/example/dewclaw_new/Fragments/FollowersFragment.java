package com.example.dewclaw_new.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dewclaw_new.Activities.OtherProfileActivity;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FollowersFragment extends Fragment {


    RecyclerView recyclerView;

    ArrayList<UserDTO> followersList;

    FireBaseModel mModel;

    Integer currentUserId;

    public FollowersFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_followers, container, false);

        mModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);
        currentUserId = mModel.getFireBaseAuthInstance().getCurrentUser().hashCode();

        Bundle arguments = this.getArguments();
        followersList = arguments.getParcelableArrayList(ProfileFragment.FollowersBundleKey);
        if(followersList == null)
            followersList = new ArrayList<>();

        recyclerView = view.findViewById(R.id.followers_recycler_iv);

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        recyclerView.setAdapter(new RVAdapterFollowers(followersList,getContext()));

        return view;
    }

    public class RVAdapterFollowers extends RecyclerView.Adapter<RVAdapterFollowers.FollowersViewHolder>{

        Context mContext;
        List<UserDTO> followersList;

        public RVAdapterFollowers(List<UserDTO> followers, Context context){
            followersList = followers;
            mContext = context;
        }

        @NonNull
        @Override
        public FollowersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.followers_and_following_content, parent,false);
            FollowersFragment.RVAdapterFollowers.FollowersViewHolder viewHolder = new FollowersFragment.RVAdapterFollowers.FollowersViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull FollowersViewHolder holder, int position) {
            UserDTO userDTO = followersList.get(position);

            holder.setDetails(userDTO);
        }

        @Override
        public int getItemCount() {
            if(followersList != null)
                return followersList.size();
            return 0;
        }

        public class FollowersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            UserDTO userDTO;

            View view;

            ImageView followers_and_following_content_iv;
            TextView followers_and_following_content_tv;

            public FollowersViewHolder(@NonNull View itemView) {
                super(itemView);

                view = itemView;
            }

            public void setDetails(UserDTO follower){
                userDTO = follower;

                followers_and_following_content_iv = view.findViewById(R.id.followers_and_following_content_iv);
                followers_and_following_content_tv = view.findViewById(R.id.followers_and_following_content_tv);

                Picasso.with(MyApplication.getContext()).load(userDTO.getProfileImage()).into(followers_and_following_content_iv);
                followers_and_following_content_tv.setText(userDTO.getFullName());

                followers_and_following_content_tv.setOnClickListener(this);
                followers_and_following_content_iv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if(userDTO.getId().equals(currentUserId)){
                    Fragment fragment = new ProfileFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(FollowersFragment.this.getId(),fragment);
                    transaction.commit();
                }
                else{
                    Intent intent = new Intent(MyApplication.getContext(), OtherProfileActivity.class);
                    intent.putExtra("UserId", userDTO.getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getContext().startActivity(intent);
                }
            }
        }
    }
}
