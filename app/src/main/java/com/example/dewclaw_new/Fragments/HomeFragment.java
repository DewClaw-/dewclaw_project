package com.example.dewclaw_new.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dewclaw_new.Activities.MainActivity;
import com.example.dewclaw_new.Activities.OtherProfileActivity;
import com.example.dewclaw_new.Activities.PostPageActivity;
import com.example.dewclaw_new.DTOs.CommentDTO;
import com.example.dewclaw_new.DTOs.PostDTO;
import com.example.dewclaw_new.DTOs.ProfilePictureDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Handlers.UserProfilePictureHandler;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.os.SystemClock.sleep;


public class HomeFragment extends Fragment {

    CircleImageView home_user_profile_picture_iv;
    TextView home_username_tv;
    Button home_new_post_btn;
    LinearLayout home_fragment;


    RecyclerView recyclerView;


    String userId;

    String showComments = "Show Comments...";
    String hideComments = "Hide Comments...";

    FireBaseModel mModel;
    ModelManager modelManager;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        home_user_profile_picture_iv = view.findViewById(R.id.home_user_profile_picture_iv);
        home_username_tv = view.findViewById(R.id.home_username_tv);
        home_new_post_btn = view.findViewById(R.id.home_new_post_btn);
        home_fragment = view.findViewById(R.id.home_fragment);

//        home_fragment.setBackgroundResource(R.drawable.background_paws);

        mModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);
        modelManager = new ModelManager();
        userId = mModel.getFireBaseAuthInstance().getCurrentUser().getUid();


        recyclerView = view.findViewById(R.id.home_recycle_view);

        UserProfilePictureHandler.getUserProfilePicture(userId, new IGlobalListener<ProfilePictureDTO>() {
            @Override
            public void onComplete(ProfilePictureDTO object) {
                if(object != null){
                    if(object.NeedDefault){

                    }
                    else{
                        Picasso.with(MyApplication.getContext()).load(object.ExternalPicture).into(home_user_profile_picture_iv);

                    }
                }
            }
        });

        modelManager.getUserById(userId, new IGetUserListener() {
            @Override
            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    home_username_tv.setText(""+userDTO.getUserName());
                }
            }
        });

        home_new_post_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(HomeFragment.this.getActivity(), "Please wait...", Toast.LENGTH_SHORT).show();
                new CountDownTimer(2000,2000) {

                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        MoveToNewPost();
                    }
                }.start();
            }
        });

        mModel.getUserHomePosts(userId, new IGlobalListener<ArrayList<PostDTO>>() {
            @Override
            public void onComplete(ArrayList<PostDTO> posts) {
                if(posts != null){
                    recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));
                    recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
                    recyclerView.setAdapter(new RVAdapterHome(posts,getContext()));
                    recyclerView.setNestedScrollingEnabled(false);
                }
            }
        });

        home_user_profile_picture_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoveToProfilePage();
            }
        });

        return view;
    }

    private void MoveToNewPost() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        UploadPostFragment uploadPostFragment = new UploadPostFragment();
        fragmentTransaction.replace(this.getId(),uploadPostFragment);
        fragmentTransaction.commit();
    }

    private void MoveToProfilePage(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ProfileFragment uploadPostFragment = new ProfileFragment();
        fragmentTransaction.replace(this.getId(),uploadPostFragment);
        fragmentTransaction.commit();
    }



    public class RVAdapterHome extends RecyclerView.Adapter<HomeFragment.RVAdapterHome.HomePostViewHolder>{

        Context mContext;
        List<PostDTO> postDTOList;

        public RVAdapterHome(List<PostDTO> posts, Context context){
            mContext = context;
            postDTOList = posts;
        }

        @NonNull
        @Override
        public HomePostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.home_post_content, parent,false);
            HomePostViewHolder viewHolder = new HomePostViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull HomePostViewHolder holder, int position) {
            PostDTO postDTO = postDTOList.get(position);

            holder.setDetails(postDTO);
        }

        @Override
        public int getItemCount() {
            if(postDTOList != null)
                return postDTOList.size();
            return 0;
        }

        public class HomePostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            View view;

            RecyclerView innerRecyclerView;

            PostDTO postDTO;
            UserDTO postedUserDTO;

            TextView user_post_show_comments_tv;


            public HomePostViewHolder(@NonNull View itemView) {
                super(itemView);

                view = itemView;
            }

            public void setDetails(PostDTO givenPostDTO){
                postDTO = givenPostDTO;

                innerRecyclerView = view.findViewById(R.id.home_comments_recycle_view);

                final CircleImageView user_profile_image_home_content = view.findViewById(R.id.user_profile_image_home_content);
                final TextView username_home_content = view.findViewById(R.id.username_home_content);
                final ImageView user_post_image_home_content = view.findViewById(R.id.user_post_image_home_content);
                final TextView user_post_text_home_content = view.findViewById(R.id.user_post_text_home_content);
                Button btn_like_home_page = view.findViewById(R.id.btn_like_home_page);
                final TextView num_of_likes_home_page = view.findViewById(R.id.num_of_likes_home_page);

                user_post_show_comments_tv = view.findViewById(R.id.user_post_show_comments_tv);

                modelManager.getUserById(givenPostDTO.getUserId(), new IGetUserListener() {
                    @Override
                    public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                        if(userDTO != null){
                            postedUserDTO = userDTO;
                            Picasso.with(MyApplication.getContext()).load(postedUserDTO.getProfileImage()).into(user_profile_image_home_content);
                            username_home_content.setText(""+postedUserDTO.getUserName());
                            String text = postDTO.getPostText();
                            user_post_text_home_content.setText(text);
                            Picasso.with(MyApplication.getContext()).load(postDTO.getPostImage()).into(user_post_image_home_content);
                            num_of_likes_home_page.setText(""+ postDTO.getNumberOfLikes());
                        }
                    }
                });

                mModel.getPostComments(postDTO.getPostId(), new IGlobalListener<ArrayList<CommentDTO>>() {
                    @Override
                    public void onComplete(ArrayList<CommentDTO> comments) {
                        if(comments != null){
                            Collections.sort(comments);
                            innerRecyclerView.setAdapter(new RVAdapterHomeComments(comments, getContext()));
                            innerRecyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
                            innerRecyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));
                        }
                    }
                });
//                innerRecyclerView.animate().translationY(1.0f);
                innerRecyclerView.setVisibility(View.GONE);
                user_post_show_comments_tv.setOnClickListener(this);
                btn_like_home_page.setOnClickListener(this);
                user_profile_image_home_content.setOnClickListener(this);
                user_post_image_home_content.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.btn_like_home_page){
                    mModel.likePost(userId,postDTO.getPostId(), new IGlobalListener<Boolean>() {
                        @Override
                        public void onComplete(Boolean object) {
                            if(object){
                                mModel.getPostById(postDTO.getPostId(), new IGlobalListener<PostDTO>() {
                                    @Override
                                    public void onComplete(PostDTO postFromDb) {
                                        if(postFromDb != null){
                                            setDetails(postFromDb);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
                if(v.getId() == R.id.user_profile_image_home_content){
                    if(postedUserDTO.getId().equals(userId)){
                        Fragment fragment = new ProfileFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(HomeFragment.this.getId(),fragment);
                        transaction.commit();
                    }
                    else{
                        Intent intent = new Intent(getContext(), OtherProfileActivity.class);
                        intent.putExtra("UserId", postedUserDTO.getId());
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        MyApplication.getContext().startActivity(intent);
                    }
                }
                if(v.getId() == R.id.user_post_image_home_content){
                    Intent intent = new Intent(getContext(), PostPageActivity.class);
                    intent.putExtra("PostId", postDTO.getPostId());
                    intent.putExtra("UserId", postDTO.getUserId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getContext().startActivity(intent);
                }
                if(v.getId() == R.id.user_post_show_comments_tv){
                    if(user_post_show_comments_tv.getText().toString().equals(showComments)){
                        innerRecyclerView.setVisibility(View.VISIBLE);
                        user_post_show_comments_tv.setText(hideComments);
                    }
                    else{
                        innerRecyclerView.setVisibility(View.GONE);
                        user_post_show_comments_tv.setText(showComments);
                    }
                }
            }
        }

    }
    public class RVAdapterHomeComments extends RecyclerView.Adapter<HomeFragment.RVAdapterHomeComments.HomePostViewHolderComments>{

        Context innerContext;
        List<CommentDTO> commentDTOList;

        public RVAdapterHomeComments(List<CommentDTO> comments, Context context){
            innerContext = context;
            commentDTOList = comments;
        }

        @NonNull
        @Override
        public HomePostViewHolderComments onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(innerContext)
                    .inflate(R.layout.home_comment_content, parent,false);
            HomePostViewHolderComments viewHolder = new HomePostViewHolderComments(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull HomePostViewHolderComments holder, int position) {
            CommentDTO commentDTO = commentDTOList.get(position);

            holder.setDetails(commentDTO);
        }

        @Override
        public int getItemCount() {
            if(commentDTOList != null)
                return commentDTOList.size();
            return 0;
        }

        public class HomePostViewHolderComments extends RecyclerView.ViewHolder implements View.OnClickListener {

            View view;
            CommentDTO currentComment;

            public HomePostViewHolderComments(@NonNull View itemView) {
                super(itemView);

                view = itemView;
            }

            public void setDetails(final CommentDTO commentDTO){

                currentComment = commentDTO;

                final CircleImageView comment_profile_image_home_content = view.findViewById(R.id.comment_profile_image_home_content);
                final TextView comment_username_home_content = view.findViewById(R.id.comment_username_home_content);
                final TextView comment_text_home_content = view.findViewById(R.id.comment_text_home_content);

                modelManager.getUserById(commentDTO.getUserId(), new IGetUserListener() {
                    @Override
                    public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                        if(userDTO != null){
                            Picasso.with(MyApplication.getContext()).load(userDTO.getProfileImage()).into(comment_profile_image_home_content);
                            comment_username_home_content.setText(""+userDTO.getUserName());
                            comment_text_home_content.setText(""+commentDTO.getText());
                        }
                    }
                });
                comment_profile_image_home_content.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.comment_profile_image_home_content){
                    if(currentComment.getUserId().equals(userId)){
                        Fragment fragment = new ProfileFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(HomeFragment.this.getId(),fragment);
                        transaction.commit();
                    }
                    else {
                        mModel.getPostById(currentComment.getPostId(), new IGlobalListener<PostDTO>() {
                            @Override
                            public void onComplete(PostDTO post) {
                                if(post != null){
                                    Intent intent = new Intent(getContext(), OtherProfileActivity.class);
                                    intent.putExtra("UserId", post.getUserId());
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    MyApplication.getContext().startActivity(intent);
                                }
                            }
                        });
                    }
                }
            }
        }
    }
}





















