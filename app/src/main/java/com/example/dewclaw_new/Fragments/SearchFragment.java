package com.example.dewclaw_new.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.dewclaw_new.Activities.OtherProfileActivity;
import com.example.dewclaw_new.Activities.PostPageActivity;
import com.example.dewclaw_new.DTOs.SearchContentDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import com.squareup.picasso.Picasso;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;


public class SearchFragment extends Fragment {

    EditText search_input;
    ListView search_list;
    Button search_btn;
    ArrayAdapter listAdapter;

    FireBaseModel mModel;
    RecyclerView recyclerView;

    View view;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);

        search_input = view.findViewById(R.id.search_input_et);
//        search_list = view.findViewById(R.id.search_list);
        search_btn = view.findViewById(R.id.search_btn);

        search_input.setText("");

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.user_recycle_view);

    }

    private void search() {
        mModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);

        mModel.searchContent(search_input.getText().toString(), new IGlobalListener<Set<SearchContentDTO>>() {
            @Override
            public void onComplete(Set<SearchContentDTO> object) {
                if(object != null){
                    recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));
                    recyclerView.setAdapter(new RVAdapter(new ArrayList<SearchContentDTO>(object),getContext()));
                }
            }
        });
    }

    public class RVAdapter extends RecyclerView.Adapter<RVAdapter.SearchViewHolder>{

        Context mContext;
        List<SearchContentDTO> userDTOList;

        public RVAdapter(List<SearchContentDTO> users, Context context){
            userDTOList = users;
            mContext = context;
        }

        @NonNull
        @Override
        public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.search_content, parent,false);
            SearchViewHolder viewHolder = new SearchViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
            SearchContentDTO searchContentDTO = userDTOList.get(position);

            holder.setDetails(searchContentDTO);
        }

        @Override
        public int getItemCount() {
            if(userDTOList != null)
                return userDTOList.size();
            return 0;
        }

        public class SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            View view;
            String userId;

            public SearchViewHolder(@NonNull View itemView) {
                super(itemView);

                view = itemView;
            }

            public void setDetails(SearchContentDTO searchedUserId){

                final CircleImageView image_iv = view.findViewById(R.id.search_content_profile_image);
                TextView full_name_tv = view.findViewById(R.id.search_content_full_name);
                TextView email_tv = view.findViewById(R.id.search_content_email);
                TextView dog_name_tv = view.findViewById(R.id.search_content_dog_name);

                if(searchedUserId != null){
                    if(searchedUserId.getUserDTO() != null){
                        userId = searchedUserId.getUserDTO().getId();

                        full_name_tv.setText(searchedUserId.getUserDTO().getFullName());
                        email_tv.setText(searchedUserId.getUserDTO().getEmail());
                        dog_name_tv.setText(searchedUserId.getUserDTO().getDogName());
                        Picasso.with(MyApplication.getContext()).load(searchedUserId.getUserDTO().getProfileImage()).into(image_iv);

                        image_iv.setOnClickListener(this);

                    }
                    else if(searchedUserId.getBusinessDTO() != null){
                        full_name_tv.setText(searchedUserId.getBusinessDTO().getBusinessName());

                        mModel.getUserById(searchedUserId.getBusinessDTO().getBusinessUserId(), new IGetUserListener() {
                            @Override
                            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                                if(userDTO != null){
                                    email_tv.setText(""+userDTO.getEmail());
                                    dog_name_tv.setText(""+searchedUserId.getBusinessDTO().getBusinessType());
                                    Picasso.with(MyApplication.getContext()).load(searchedUserId.getBusinessDTO().getBusinessLogo()).into(image_iv);

                                    userId = userDTO.getId();
                                    image_iv.setOnClickListener(SearchViewHolder.this);
                                }
                            }
                        });
                    }
                }

            }

            @Override
            public void onClick(View v) {

                FireBaseModel fireBaseModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);

                String currentUserId = fireBaseModel.getFireBaseAuthInstance().getCurrentUser().getUid();

                if(currentUserId.equals(userId)){
                    Fragment fragment = new ProfileFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(SearchFragment.this.getId(),fragment);
                    transaction.commit();
                }
                else{
                    Intent intent = new Intent(MyApplication.getContext(), OtherProfileActivity.class);
                    intent.putExtra("UserId", userId);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getContext().startActivity(intent);
                }
            }
        }
    }


}
