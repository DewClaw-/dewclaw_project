package com.example.dewclaw_new.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dewclaw_new.DTOs.BusinessDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Generators.StringsGenerator;
import com.example.dewclaw_new.Handlers.BusinessHandler;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import com.squareup.picasso.Picasso;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class BusinessDetailsFragment extends Fragment {

    Spinner business_details_business_type;
    Spinner business_details_business_free_paid;
    EditText business_details_little_about_us;
    Button business_update_details_btn;
    ImageView business_details_logo_iv;
    Button business_logo_rotate_btn;
    EditText business_details_business_name;
    TextView business_details_logo_tv;

    FireBaseModel mModel;
    ModelManager modelManager;


    String globalUserId;
    String globalBusinessId;
    String globalBusinessLogo;

    final int SELECT_AN_IMAGE_FROM_GALLERY = 1;
    Uri mImageUri;
    Bitmap bitmap;

    float angle = 0;


    public BusinessDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_details, container, false);

        business_details_business_type = view.findViewById(R.id.business_details_business_type);
        business_details_business_free_paid = view.findViewById(R.id.business_details_business_free_paid);
        business_details_little_about_us = view.findViewById(R.id.business_details_little_about_us);
        business_update_details_btn = view.findViewById(R.id.business_update_details_btn);
        business_details_logo_iv = view.findViewById(R.id.business_details_logo_iv);
        business_logo_rotate_btn = view.findViewById(R.id.business_logo_rotate_btn);
        business_details_business_name = view.findViewById(R.id.business_details_business_name);
        business_details_logo_tv = view.findViewById(R.id.business_details_logo_tv);

        mModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);
        modelManager = new ModelManager();

        handleBusinessTypes();
        handleBusinessPayments();

        Bundle bundle = getArguments();
        if(bundle != null){
            globalUserId = bundle.getString("UserId");
        }

        init();

        business_update_details_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateBusinessDetails();
            }
        });

        business_details_logo_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeBusinessLOGO();
            }
        });

        business_details_logo_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeBusinessLOGO();
            }
        });

        business_logo_rotate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotateLOGO();
            }
        });

        return view;
    }



    private void init(){
        modelManager.getUserById(globalUserId, new IGetUserListener() {
            @Override
            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    if(userDTO.getBusinessId() != null){
                        globalBusinessId = userDTO.getBusinessId();
                        mModel.getBusinessById(userDTO.getBusinessId(), new IGlobalListener<BusinessDTO>() {
                            @Override
                            public void onComplete(BusinessDTO businessDTO) {
                                if(businessDTO != null){
                                    business_details_business_name.setText(""+businessDTO.getBusinessName());
                                    business_details_business_type.setSelection(BusinessHandler.getBusinessTypeEnumValue(businessDTO.getBusinessType()));
                                    handleBusinessOwners(businessDTO);
                                    business_details_business_free_paid.setSelection(BusinessHandler.getBusinessPaymentEnumValue(businessDTO.getBusinessPayment()));
                                    business_details_little_about_us.setText(""+businessDTO.getBusinessAboutUs());
                                    Picasso.with(MyApplication.getContext()).load(businessDTO.getBusinessLogo()).into(business_details_logo_iv);
                                    globalBusinessLogo = businessDTO.getBusinessLogo();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void handleBusinessTypes() {
        BusinessHandler.getBusinessTypes(business_details_business_type,getActivity());
    }

    private void handleBusinessPayments(){
        BusinessHandler.getBusinessPayments(business_details_business_free_paid,getActivity());
    }

    private void handleBusinessOwners(BusinessDTO businessDTO) {
    }

    private void updateBusinessDetails() {
        final BusinessDTO businessDTO = new BusinessDTO();
        businessDTO.setBusinessAboutUs(business_details_little_about_us.getText().toString());
        //HANDLE LOGO
        businessDTO.setBusinessName(business_details_business_name.getText().toString());
        businessDTO.setBusinessPayment(BusinessHandler.getBusinessPayment(business_details_business_free_paid.getSelectedItem().toString()));
        businessDTO.setBusinessType(BusinessHandler.getBusinessType(business_details_business_type.getSelectedItem().toString()));
        businessDTO.setBusinessUserId(globalUserId);
        modelManager.getUserById(globalUserId, new IGetUserListener() {
            @Override
            public void onComplete(final UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    if (userDTO.getBusinessId() == null){
                        mModel.createNewBusiness(globalUserId,businessDTO,mImageUri, new IGlobalListener<BusinessDTO>() {
                            @Override
                            public void onComplete(BusinessDTO businessDTO) {
                                if(businessDTO != null){
                                    moveBackToBusinessPageFragment();
                                    Toast.makeText(MyApplication.getContext(),"Business has been Created!",Toast.LENGTH_LONG).show();
                                    return;
                                }
                                Toast.makeText(MyApplication.getContext(),"Any Problem with creating a new business. (updateBusinessDetails)",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    else{
                        businessDTO.setBusinessId(userDTO.getBusinessId());
                        businessDTO.setBusinessLogo(globalBusinessLogo);
                        mModel.updateBusiness(businessDTO,mImageUri, new IGlobalListener<Boolean>() {
                            @Override
                            public void onComplete(Boolean object) {
                                if(object){
                                    mModel.getBusinessById(userDTO.getBusinessId(), new IGlobalListener<BusinessDTO>() {
                                        @Override
                                        public void onComplete(BusinessDTO business) {
                                            if(business != null){
                                                moveBackToBusinessPageFragment();
                                                Toast.makeText(MyApplication.getContext(),"Business has been updated!",Toast.LENGTH_LONG).show();
                                                return;
                                            }
                                            Toast.makeText(MyApplication.getContext(),"Any Problem with updating the business. (updateBusinessDetails)",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });

    }

    private void moveBackToBusinessPageFragment() {
        Fragment fragment = new BusinessPageFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment_content, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void changeBusinessLOGO() {
        Intent intent = new Intent();    //CHOOSE A PHOTO FROM GALLERY
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,SELECT_AN_IMAGE_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case SELECT_AN_IMAGE_FROM_GALLERY:
                if(resultCode == RESULT_OK){
                    if(data != null){
                        mImageUri = data.getData();
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(MyApplication.getContext().getContentResolver(), mImageUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Picasso.with(MyApplication.getContext()).load(mImageUri).into(business_details_logo_iv);
                        business_details_logo_iv.setRotation(angle);
                    }
                }
                break;
        }
    }

    private void rotateLOGO(){
        if(mImageUri == null && ((BitmapDrawable)business_details_logo_iv.getDrawable()).getBitmap() == null)
            Toast.makeText(getContext(), "No picture has been chosen.", Toast.LENGTH_SHORT).show();
        else if(mImageUri == null && ((BitmapDrawable)business_details_logo_iv.getDrawable()).getBitmap() != null){
            bitmap = rotateBitmap(((BitmapDrawable)business_details_logo_iv.getDrawable()).getBitmap(),angle-=90) ;
            business_details_logo_iv.setImageBitmap(bitmap);
        }
        else{
            Log.d("TAG","Rotation :: Rotation ::");
            business_details_logo_iv.setRotation(angle);
            angle -= 90;
            bitmap = rotateBitmap(((BitmapDrawable)business_details_logo_iv.getDrawable()).getBitmap(),angle);
            business_details_logo_iv.setImageBitmap(bitmap);
            File file = new File(mImageUri.getPath());
            if(file.exists()){
                OutputStream os = null;
                try {
                    os = new BufferedOutputStream(new FileOutputStream(file));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mImageUri = Uri.fromFile(file);
            }
        }
    }

    private Bitmap rotateBitmap(Bitmap bitmap,float angle) {
        Matrix matrix = new Matrix();
        matrix.setRotate(angle);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);
    }

}

