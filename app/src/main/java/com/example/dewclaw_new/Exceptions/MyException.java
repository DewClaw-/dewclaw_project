package com.example.dewclaw_new.Exceptions;

public class MyException extends Exception {

    public MyException() {}

    // Constructor that accepts a message
    public MyException(String message)
    {
        super(message);
    }
}
