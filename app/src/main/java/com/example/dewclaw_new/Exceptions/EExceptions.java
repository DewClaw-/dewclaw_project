package com.example.dewclaw_new.Exceptions;

public enum EExceptions {
    EmailValidation,
    BadPasswordConfirmation,
    AlreadyExistUser,
    WrongUsernameOrPassword,
    EmptyFieldMessage
}
