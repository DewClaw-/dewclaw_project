package com.example.dewclaw_new.Exceptions;

import com.example.dewclaw_new.DTOs.ExceptionDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.Intefraces.IGetAllExceptionsListener;
import com.example.dewclaw_new.Models.Intefraces.IGetExceptionListener;
import com.example.dewclaw_new.Models.ModelManager;

import java.util.HashMap;

public class ExceptionsUtil {

    private static HashMap<EExceptions,String> exceptionsMap = new HashMap<>();

    private static void init(final IGlobalListener<Boolean> listener){

        FireBaseModel fireBaseModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);

        fireBaseModel.getAllExceptions(new IGetAllExceptionsListener() {
            @Override
            public void onComplete(HashMap<EExceptions, String> map) {
                if(map != null){
                    exceptionsMap = map;
                    listener.onComplete(true);
                }
                else{
                    listener.onComplete(false);
                }
            }
        });
    }

    public static void getByKey(final EExceptions exceptionKey, final IGetExceptionListener listener){
        if(exceptionsMap.isEmpty()){
            init(new IGlobalListener<Boolean>() {
                @Override
                public void onComplete(Boolean finished) {
                    if(finished)
                        listener.onComplete(new ExceptionDTO(exceptionKey,exceptionsMap.get(exceptionKey)));
                }
            });

        }
        else
            listener.onComplete(new ExceptionDTO(exceptionKey,exceptionsMap.get(exceptionKey)));
    }
}
