package com.example.dewclaw_new.Models;

import android.content.Context;
import android.net.ConnectivityManager;

import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IModel;
import com.example.dewclaw_new.MyApplication;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static androidx.core.content.ContextCompat.getSystemService;

public class ModelManager {

    private static Context context;

    public ModelManager() {
        context = MyApplication.getContext();
    }


    public static IModel getInstance(EModel eModel) {
        switch (eModel) {
            case FireBase:
                return FireBaseModel.Instance;
            case SqlLite:
                return SqlLiteModel.Instance;
            default:
                return null;
        }
    }

    public boolean checkConnection() {
        return true;
//        ConnectivityManager cm = (ConnectivityManager) MyApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//
//        return cm.getActiveNetworkInfo() != null;
    }

    public void getUserById(String idToCheck, IGetUserListener listener) {
        if (checkConnection()) {
            FireBaseModel fireBaseModel = (FireBaseModel) getInstance(EModel.FireBase);
            fireBaseModel.getUserById(idToCheck, listener);
        } else {
            SqlLiteModel sqlLiteModel = (SqlLiteModel) getInstance(EModel.SqlLite);
            sqlLiteModel.getUserById(idToCheck, listener);
        }
    }
}
