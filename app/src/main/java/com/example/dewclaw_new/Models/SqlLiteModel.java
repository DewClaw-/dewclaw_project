package com.example.dewclaw_new.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.icu.text.DisplayContext;
import android.net.Uri;
import android.widget.Toast;

import com.example.dewclaw_new.DTOs.BusinessDTO;
import com.example.dewclaw_new.DTOs.CommentDTO;
import com.example.dewclaw_new.DTOs.FieldAndTypeDTO;
import com.example.dewclaw_new.DTOs.PostDTO;
import com.example.dewclaw_new.DTOs.SearchContentDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EBusinessPayment;
import com.example.dewclaw_new.Enums.EBusinessType;
import com.example.dewclaw_new.Enums.EBusinessesFields;
import com.example.dewclaw_new.Enums.ECommentsFields;
import com.example.dewclaw_new.Enums.EExceptionsFields;
import com.example.dewclaw_new.Enums.EPostsFields;
import com.example.dewclaw_new.Enums.ESqlTypes;
import com.example.dewclaw_new.Enums.ETables;
import com.example.dewclaw_new.Enums.EUsersFields;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Helpers.MySqliteHelper;
import com.example.dewclaw_new.Models.Intefraces.IGetAllExceptionsListener;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.Intefraces.IModel;
import com.example.dewclaw_new.MyApplication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import androidx.annotation.Nullable;

public class SqlLiteModel implements IModel {

    final public static SqlLiteModel Instance = new SqlLiteModel();

    MySqliteHelper mySqliteHelper;
    SQLiteDatabase database;

    private SqlLiteModel(){
        mySqliteHelper = new MySqliteHelper(MyApplication.getContext());
    }


    @Override
    public void logUserOut() {
        Toast.makeText(MyApplication.getContext(),"Any Problem with logging out. No internet connection",Toast.LENGTH_LONG).show();
    }

    @Override
    public void createNewUser(UserDTO userDTO, OnCompleteListener listener) throws MyException, NoSuchAlgorithmException {

        getUserById(userDTO.getId(), userFromDb -> {
            if(userFromDb == null){
                database = mySqliteHelper.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put(EUsersFields.id.name(), userDTO.getId());
                contentValues.put(EUsersFields.email.name(),userDTO.getEmail());
                contentValues.put(EUsersFields.password.name(),userDTO.getPassword());
                contentValues.put(EUsersFields.userName.name(),userDTO.getUserName());
                contentValues.put(EUsersFields.firstName.name(),userDTO.getFirstName());
                contentValues.put(EUsersFields.lastName.name(),userDTO.getLastName());
                contentValues.put(EUsersFields.profileImage.name(),userDTO.getProfileImage());
                contentValues.put(EUsersFields.age.name(),userDTO.getAge());
                contentValues.put(EUsersFields.dogName.name(),userDTO.getDogName());
                contentValues.put(EUsersFields.dogBreed.name(),userDTO.getDogBreed());
                contentValues.put(EUsersFields.fullName.name(),userDTO.getFullName());
                contentValues.put(EUsersFields.businessId.name(),userDTO.getBusinessId());

                Gson gson = new Gson();
                contentValues.put(EUsersFields.following.name(),gson.toJson(userDTO.getFollowing()));
                contentValues.put(EUsersFields.followers.name(),gson.toJson(userDTO.getFollowers()));

                database.insert(ETables.Users.name(),EUsersFields.id.name(),contentValues);
            }
        });
    }

    @Override
    public void getUserById(String idToCheck, IGetUserListener listener) {
        database = mySqliteHelper.getReadableDatabase();

        Cursor cursor = database.rawQuery("select * from " + ETables.Users.name() + " where " + EUsersFields.id.name() + " = " + "'" + idToCheck + "'", null);

        if(cursor.moveToFirst()){
            UserDTO userDTO = new UserDTO();
            userDTO.setId(cursor.getString(cursor.getColumnIndex(EUsersFields.id.name())));
            userDTO.setEmail(cursor.getString(cursor.getColumnIndex(EUsersFields.email.name())));
            userDTO.setPassword(cursor.getString(cursor.getColumnIndex(EUsersFields.password.name())));
            userDTO.setUserName(cursor.getString(cursor.getColumnIndex(EUsersFields.userName.name())));
            userDTO.setFirstName(cursor.getString(cursor.getColumnIndex(EUsersFields.firstName.name())));
            userDTO.setLastName((cursor.getString(cursor.getColumnIndex(EUsersFields.lastName.name()))));
            userDTO.setProfileImage((cursor.getString(cursor.getColumnIndex(EUsersFields.profileImage.name()))));
            userDTO.setAge((cursor.getString(cursor.getColumnIndex(EUsersFields.age.name()))));
            userDTO.setDogName((cursor.getString(cursor.getColumnIndex(EUsersFields.dogName.name()))));
            userDTO.setDogAge((cursor.getString(cursor.getColumnIndex(EUsersFields.dogAge.name()))));
            userDTO.setDogBreed((cursor.getString(cursor.getColumnIndex(EUsersFields.dogBreed.name()))));
            userDTO.setFullName((cursor.getString(cursor.getColumnIndex(EUsersFields.fullName.name()))));
            userDTO.setBusinessId((cursor.getString(cursor.getColumnIndex(EUsersFields.businessId.name()))));

            Gson gson = new Gson();

            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();

            userDTO.setFollowing(gson.fromJson(cursor.getString(cursor.getColumnIndex(EUsersFields.following.name())),type));
            userDTO.setFollowers(gson.fromJson(cursor.getString(cursor.getColumnIndex(EUsersFields.followers.name())),type));

            try {
                listener.onComplete(userDTO);
            } catch (MyException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                listener.onComplete(null);
            } catch (MyException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void logUserIn(String email, String password, OnCompleteListener listener) throws MyException {

    }

    @Override
    public void uploadImage(String imageToString, Uri path, String postText, String extension, IGlobalListener<Boolean> listener) {

    }

    @Override
    public void getAllUsers(IGlobalListener<ArrayList<UserDTO>> listener) {

    }

    @Override
    public void getAllExceptions(IGetAllExceptionsListener listener) {

    }

    @Override
    public void getUserProfilePicture(String userId, IGlobalListener<String> listener) {

    }

    @Override
    public void uploadProfilePictureImage(String name, Uri uri, String extension, IGlobalListener<Boolean> listener) {

    }

    @Override
    public void updateUserDetails(UserDTO userDTO, IGlobalListener<UserDTO> listener) {

    }

    @Override
    public void searchContent(String inputText, IGlobalListener<Set<SearchContentDTO>> listener) {

    }

    @Override
    public void getUserFollowing(String userId, IGlobalListener<Set<UserDTO>> listener) {

    }

    @Override
    public void getUserFollowers(String userId, IGlobalListener<Set<UserDTO>> listener) {

    }

    @Override
    public void getAllPosts(IGlobalListener<ArrayList<PostDTO>> listener) {

    }

    @Override
    public void getUserPosts(String userId, IGlobalListener<Set<PostDTO>> listener) {

    }

    @Override
    public void getPostById(String postId, IGlobalListener<PostDTO> listener) {

    }

    @Override
    public void getAllComments(IGlobalListener<ArrayList<CommentDTO>> listener) {

    }

    @Override
    public void getPostComments(String postId, IGlobalListener<ArrayList<CommentDTO>> listener) {

    }

    @Override
    public void likePost(String userId, String postId, IGlobalListener<Boolean> listener) {

    }

    @Override
    public void commentOnPost(String userId, String postId, CommentDTO commentDTO, IGlobalListener<Boolean> listener) {

    }

    @Override
    public void followThisProfile(String userId, IGlobalListener<String> listener) {

    }

    @Override
    public void getUserHomePosts(String userId, IGlobalListener<ArrayList<PostDTO>> listener) {

    }

    @Override
    public void deletePost(String postId, IGlobalListener<Boolean> listener) {

    }

    @Override
    public void createNewBusiness(String userId, BusinessDTO businessDTO, Uri mImageUri, IGlobalListener<BusinessDTO> listener) {

    }

    @Override
    public void getBusinessById(String businessId, IGlobalListener<BusinessDTO> listener) {

    }

    @Override
    public void getAllBusinessTypes(IGlobalListener<HashMap<EBusinessType, String>> listener) {

    }

    @Override
    public void getAllBusinessPayments(IGlobalListener<HashMap<EBusinessPayment, String>> listener) {

    }

    @Override
    public void updateBusiness(BusinessDTO businessDTO, Uri mImageUri, IGlobalListener<Boolean> listener) {

    }

    @Override
    public void uploadBusinessLOGO(String businessId, String logoName, Uri mImageUri, String extension, IGlobalListener<Boolean> listener) {

    }


}
