package com.example.dewclaw_new.Models;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.dewclaw_new.Activities.OtherProfileActivity;
import com.example.dewclaw_new.DTOs.BusinessDTO;
import com.example.dewclaw_new.DTOs.CommentDTO;
import com.example.dewclaw_new.DTOs.PostDTO;
import com.example.dewclaw_new.DTOs.SearchContentDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EBusinessPayment;
import com.example.dewclaw_new.Enums.EBusinessType;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Enums.ETables;
import com.example.dewclaw_new.Enums.EUsersFields;
import com.example.dewclaw_new.Exceptions.EExceptions;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Generators.StringsGenerator;
import com.example.dewclaw_new.Handlers.BusinessHandler;
import com.example.dewclaw_new.Helpers.MySqliteHelper;
import com.example.dewclaw_new.Models.Intefraces.IGetAllExceptionsListener;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.Intefraces.IModel;
import com.example.dewclaw_new.MyApplication;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.model.Document;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static androidx.core.content.ContextCompat.getSystemService;

public class FireBaseModel implements IModel {

    final public static FireBaseModel Instance = new FireBaseModel();



    FirebaseFirestore dataBase;

    FirebaseStorage firebaseStorage;
    StorageReference storageReference;



    DatabaseReference firebaseDatabase;

    private String usersTable = ETables.Users.name();
    private String postsTable = ETables.Posts.name();
    private String commentsTable = ETables.Comments.name();
    private String businessesTable = ETables.Businesses.name();
    private String businessTypesTable = ETables.BusinessTypes.name();
    private String businessPaymentsTable = ETables.BusinessPayment.name();

    private String profileImageField = EUsersFields.profileImage.name();
    private String emailField = EUsersFields.email.name();
    private String firstNameField = EUsersFields.firstName.name();
    private String lastNameField = EUsersFields.lastName.name();
    private String ageField = EUsersFields.age.name();
    private String dogNameField = EUsersFields.dogName.name();
    private String dogAgeField = EUsersFields.dogAge.name();
    private String dogBreedField = EUsersFields.dogBreed.name();
    private String fullNameField = EUsersFields.fullName.name();
    private String likesField = "likes";
    private String followersField = EUsersFields.followers.name();
    private String followingField = EUsersFields.following.name();
    private String numberOfLikesField = "numberOfLikes";
    private String businessIdField = EUsersFields.businessId.name();
    private String businessNameField = "businessName";
    private String businessOwnersField = "businessOwners";
    private String businessTypeField = "businessType";
    private String businessPaymentField = "businessPayment";
    private String businessAboutUsField = "businessAboutUs";
    private String businessLogoField = "businessLogo";

    private static String exceptionsTable;

    SqlLiteModel sqlLiteModel;

    public FirebaseFirestore getDataBase() {
        return dataBase;
    }

    private FireBaseModel(){
        dataBase = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings
                .Builder().setPersistenceEnabled(false).build();
        dataBase.setFirestoreSettings(settings);

        exceptionsTable = ETables.Exceptions.name();


        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReferenceFromUrl("gs://dewclawproject.appspot.com");

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        sqlLiteModel = (SqlLiteModel) ModelManager.getInstance(EModel.SqlLite);
    }

    public FirebaseAuth getFireBaseAuthInstance(){
        return FirebaseAuth.getInstance();
    }

    @Override
    public void createNewUser(final UserDTO userDTO, final OnCompleteListener listener) throws MyException {

        FirebaseAuth firebaseAuth = getFireBaseAuthInstance();

        firebaseAuth.createUserWithEmailAndPassword(userDTO.getEmail(),userDTO.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull final Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            getDefaultProfileImage(new IGlobalListener<String>() {
                                @Override
                                public void onComplete(String object) {
                                    if(object != null){
                                        userDTO.setProfileImage(object);
                                        userDTO.setFollowers(new ArrayList<String>());
                                        userDTO.setFollowing(new ArrayList<String>());
                                        userDTO.setId(task.getResult().getUser().getUid());
                                        dataBase.collection(usersTable).document(userDTO.getId()).set(userDTO);
                                        listener.onComplete(task);
                                    }
                                    else
                                        listener.onComplete(task);
                                }
                            });
                        }
                        else{
                            listener.onComplete(task);
                        }
                    }
                });

    }

    private void getDefaultProfileImage(final IGlobalListener<String> listener) {
        storageReference.child("/Profile_Images").child("default_profile_image.jpg").getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if(task.isSuccessful()){
                    String path = task.getResult().toString();
                    listener.onComplete(path);
                }
                else{
                    listener.onComplete("");
                }
            }
        });
    }

    private void getDefaultBusinessImage(final IGlobalListener<String> listener) {
        storageReference.child("/Businesses").child("default_business_image.jpg").getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if(task.isSuccessful()){
                    String path = task.getResult().toString();
                    listener.onComplete(path);
                }
                else{
                    listener.onComplete("");
                }
            }
        });
    }

    public void getUserById(String idToCheck, final IGetUserListener listener) {
        dataBase.collection(usersTable).document(idToCheck).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot snapshot = task.getResult();
                    if(snapshot.exists()){
                        UserDTO user = snapshot.toObject(UserDTO.class);
                        try {
                            listener.onComplete(user);
                        } catch (MyException | NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                        return;
                    }
                }
                try {
                    listener.onComplete(null);
                } catch (MyException | NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void logUserIn(String email, String password,final OnCompleteListener listener) throws MyException {
        final FirebaseAuth fireBaseAuth = getFireBaseAuthInstance();

        fireBaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    fireBaseAuth.updateCurrentUser(task.getResult().getUser());
                }
                listener.onComplete(task);
            }
        });
    }

    @Override
    public void logUserOut() {
        getFireBaseAuthInstance().signOut();
    }

    public void getAllUsers(final IGlobalListener<ArrayList<UserDTO>> listener){
        dataBase.collection(usersTable).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    List<DocumentSnapshot> snapshots = task.getResult().getDocuments();
                    ArrayList<UserDTO> users = new ArrayList<UserDTO>();
                    for(DocumentSnapshot doc : snapshots){
                        users.add(doc.toObject(UserDTO.class));
                    }
                    listener.onComplete(users);
                    return;
                }
                listener.onComplete(null);
            }
        });
    }

    public void uploadImage(final String imageName, final Uri imageUri , final String postText, final String extension, final IGlobalListener<Boolean> listener) throws NoSuchAlgorithmException {

        final StorageReference fileReference = storageReference.child("Posts/"+imageName+"."+extension);
        final UploadTask uploadTask = fileReference.putFile(imageUri);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if(task.isSuccessful()){
                    UploadPostDataToDB(imageName+"."+extension, postText, uploadTask, new IGlobalListener<Boolean>() {
                        @Override
                        public void onComplete(Boolean object) {
                            listener.onComplete(object);
                        }
                    });
                }
                else{
                    listener.onComplete(false);
                }
            }
        });
    }

    private void UploadPostDataToDB(final String imageFullName, final String postText, UploadTask uploadTask, final IGlobalListener<Boolean> listener) {

        final String postId = StringsGenerator.GenerateNewUUID();
        final String userId = getFireBaseAuthInstance().getCurrentUser().getUid();

        final PostDTO postDTO = new PostDTO();
        postDTO.setPostId(postId);
        postDTO.setDateOfPost(new Date());
        postDTO.setLikes(new ArrayList<String>());
        postDTO.setNumberOfLikes(0);

        uploadTask.continueWithTask(new Continuation() {
            @Override
            public Object then(@NonNull Task task) throws Exception {
                if (!task.isSuccessful()) {
                    throw  task.getException();
                }
                return storageReference.child("Posts/"+imageFullName).getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if(task.isSuccessful()){
                    postDTO.setPostImage(task.getResult().toString());
                    if(postText == null)
                        postDTO.setPostText("");
                    else
                        postDTO.setPostText(postText);
                    postDTO.setUserId(userId);
                    dataBase.collection(postsTable).document(postId).set(postDTO).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful())
                                listener.onComplete(true);
                            else
                                listener.onComplete(false);
                        }
                    });
                }
                else{
                    listener.onComplete(false);
                }
            }
        });
    }

    public void getAllExceptions(final IGetAllExceptionsListener listener){
        dataBase.collection(ETables.Exceptions.name()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {

            HashMap<EExceptions,String> map = new HashMap<>();

            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                        map.put(EExceptions.values()[Integer.parseInt(doc.getId())-1],(String) doc.get("Text"));
                    }
                    listener.onComplete(map);
                    return;
                }
            }
        });
    }

    public void getUserProfilePicture(String userId, final IGlobalListener<String> listener) {
        getUserById(userId, new IGetUserListener() {
            @Override
            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    if(userDTO.getProfileImage() != null) {
                        listener.onComplete(userDTO.getProfileImage());
                    }
                    else
                        listener.onComplete(null);
                }
            }
        });
    }

    public void uploadProfilePictureImage(final String name ,Uri uri , String extension, final IGlobalListener<Boolean> listener){
        final StorageReference fileReference = storageReference.child("Profile_Images/"+name+"."+extension);
        UploadTask uploadTask = fileReference.putFile(uri);
        uploadTask.continueWithTask(new Continuation() {
            @Override
            public Object then(@NonNull Task task) throws Exception {
                if (!task.isSuccessful()) {
                    throw  task.getException();
                }
                return fileReference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if(task.isSuccessful()){
                    String userId = getFireBaseAuthInstance().getCurrentUser().getUid();
                    DocumentReference documentReference = FirebaseFirestore.getInstance().collection(usersTable).document(userId);
                    documentReference.update(profileImageField,task.getResult().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful())
                                listener.onComplete(true);
                            else
                                listener.onComplete(false);
                        }
                    });

                }
                else{
                    listener.onComplete(false);
                }
            }
        });
    }

    public void updateUserDetails(final UserDTO userDTO, final IGlobalListener<UserDTO> listener) {
        final String userId = getFireBaseAuthInstance().getCurrentUser().getUid();
        userDTO.setId(userId);
        DocumentReference documentReference = FirebaseFirestore.getInstance().collection(usersTable).document(userId);
        HashMap<String,Object> hashMap = new HashMap();
        hashMap.put(firstNameField, userDTO.getFirstName());
        hashMap.put(lastNameField, userDTO.getLastName());
        hashMap.put(fullNameField, userDTO.getFirstName()+" "+userDTO.getLastName());
        hashMap.put(ageField, userDTO.getAge());
        hashMap.put(dogNameField, userDTO.getDogName());
        hashMap.put(dogAgeField, userDTO.getDogAge());
        hashMap.put(dogBreedField, userDTO.getDogBreed());
        documentReference.update(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    listener.onComplete(userDTO);
                    final IModel sqlModel = ModelManager.getInstance(EModel.SqlLite);
                    try {
                        sqlModel.getUserById(userId, new IGetUserListener() {
                            @Override
                            public void onComplete(UserDTO sqliteUserDTO) throws MyException, NoSuchAlgorithmException {
                                if(sqliteUserDTO != null){
                                    sqlModel.updateUserDetails(userDTO, (userFromDb)->{});
                                }else {
                                    sqlModel.createNewUser(userDTO,(task)->{});
                                }
                            }
                        });
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (MyException e) {
                        e.printStackTrace();
                    }
                }
                else
                    listener.onComplete(null);
            }
        });
    }

    public void searchContent(final String inputText, final IGlobalListener<Set<SearchContentDTO>> listener) {

        final Set<SearchContentDTO> searchContentDTOSet = new HashSet<>();
        getAllUsers(new IGlobalListener<ArrayList<UserDTO>>() {
            @Override
            public void onComplete(final ArrayList<UserDTO> users) {
                if (users != null) {
                    getAllBusnisses(new IGlobalListener<ArrayList<BusinessDTO>>() {
                        @Override
                        public void onComplete(ArrayList<BusinessDTO> businesses) {
                            if(businesses != null){
                                for (UserDTO user : users) {
                                    if (user != null && !searchContentDTOSet.contains(user)) {
                                        if (user.getFirstName() != null && user.getFirstName().toLowerCase().contains(inputText.toLowerCase())) {
                                            SearchContentDTO searchContentDTO = new SearchContentDTO();
                                            searchContentDTO.setUserDTO(user);
                                            searchContentDTOSet.add(searchContentDTO);
                                        }
                                        if (user.getLastName() != null && user.getLastName().toLowerCase().contains(inputText.toLowerCase())) {
                                            SearchContentDTO searchContentDTO = new SearchContentDTO();
                                            searchContentDTO.setUserDTO(user);
                                            searchContentDTOSet.add(searchContentDTO);
                                        }
                                        if (user.getEmail() != null && user.getEmail().toLowerCase().contains(inputText.toLowerCase())) {
                                            SearchContentDTO searchContentDTO = new SearchContentDTO();
                                            searchContentDTO.setUserDTO(user);
                                            searchContentDTOSet.add(searchContentDTO);
                                        }
                                        if (user.getDogName() != null && user.getDogName().toLowerCase().contains(inputText.toLowerCase())) {
                                            SearchContentDTO searchContentDTO = new SearchContentDTO();
                                            searchContentDTO.setUserDTO(user);
                                            searchContentDTOSet.add(searchContentDTO);
                                        }
                                        if (user.getDogBreed() != null && user.getDogBreed().toLowerCase().contains(inputText.toLowerCase())) {
                                            SearchContentDTO searchContentDTO = new SearchContentDTO();
                                            searchContentDTO.setUserDTO(user);
                                            searchContentDTOSet.add(searchContentDTO);
                                        }
                                    }
                                }
                                for(BusinessDTO business : businesses){
                                    if(business.getBusinessName() != null && business.getBusinessName().toLowerCase().contains(inputText.toLowerCase())){
                                        SearchContentDTO searchContentDTO = new SearchContentDTO();
                                        searchContentDTO.setBusinessDTO(business);
                                        searchContentDTOSet.add(searchContentDTO);
                                    }
                                    if(business.getBusinessType() != null && BusinessHandler.getBusinessTypeEnumName(business.getBusinessType()).toLowerCase().contains(inputText.toLowerCase())){
                                        SearchContentDTO searchContentDTO = new SearchContentDTO();
                                        searchContentDTO.setBusinessDTO(business);
                                        searchContentDTOSet.add(searchContentDTO);
                                    }
                                    if(business.getBusinessPayment() != null && BusinessHandler.getBusinessPaymentEnumName(business.getBusinessPayment()).toLowerCase().contains(inputText.toLowerCase())){
                                        SearchContentDTO searchContentDTO = new SearchContentDTO();
                                        searchContentDTO.setBusinessDTO(business);
                                        searchContentDTOSet.add(searchContentDTO);
                                    }
                                }
                                listener.onComplete(searchContentDTOSet);
                            }
                        }
                    });

                }
            }
        });
    }

    public void getUserFollowing(String userId, final IGlobalListener<Set<UserDTO>> listener) {

        final Set<UserDTO> following = new HashSet<>();

        getUserById(userId, new IGetUserListener() {
            @Override
            public void onComplete(final UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    getAllUsers(new IGlobalListener<ArrayList<UserDTO>>() {
                        @Override
                        public void onComplete(ArrayList<UserDTO> users) {
                            if(users != null){
                                for(UserDTO user : users){
                                    if(userDTO.getFollowing().contains(user.getId()))
                                        following.add(user);
                                }
                                listener.onComplete(following);
                            }
                        }
                    });
                }
            }
        });
    }

    public void getUserFollowers(String userId, final IGlobalListener<Set<UserDTO>> listener) {

        final Set<UserDTO> following = new HashSet<>();

        getUserById(userId, new IGetUserListener() {
            @Override
            public void onComplete(final UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    getAllUsers(new IGlobalListener<ArrayList<UserDTO>>() {
                        @Override
                        public void onComplete(ArrayList<UserDTO> users) {
                            if(users != null){
                                for(UserDTO user : users){
                                    if(userDTO.getFollowers().contains(user.getId()))
                                        following.add(user);
                                }
                                listener.onComplete(following);
                            }
                        }
                    });
                }
            }
        });
    }

    public void getAllPosts(final IGlobalListener<ArrayList<PostDTO>> listener){
        dataBase.collection(postsTable).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    List<DocumentSnapshot> snapshots = task.getResult().getDocuments();
                    ArrayList<PostDTO> posts = new ArrayList<>();
                    for(DocumentSnapshot doc : snapshots){
                        posts.add(doc.toObject(PostDTO.class));
                    }
                    listener.onComplete(posts);
                    return;
                }
                listener.onComplete(null);
            }
        });
    }

    public void getUserPosts(final String userId , final IGlobalListener<Set<PostDTO>> listener){

        final Set<PostDTO> postDTOSet = new HashSet<>();

        getAllPosts(new IGlobalListener<ArrayList<PostDTO>>() {
            @Override
            public void onComplete(ArrayList<PostDTO> posts) {
                if(posts != null){
                    for(PostDTO post : posts){
                        if(post.getUserId().equals(userId))
                            postDTOSet.add(post);
                    }
                    listener.onComplete(postDTOSet);
                }
            }
        });
    }

    public void getPostById(final String postId, final IGlobalListener<PostDTO> listener){
        getAllPosts(new IGlobalListener<ArrayList<PostDTO>>() {
            @Override
            public void onComplete(ArrayList<PostDTO> posts) {
                if(posts != null){
                    for(PostDTO post : posts){
                        if(post.getPostId().equals(postId)){
                            listener.onComplete(post);
                        }
                    }
                }
            }
        });
    }

    public void getAllComments(final IGlobalListener<ArrayList<CommentDTO>> listener){
        dataBase.collection(commentsTable).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    List<DocumentSnapshot> snapshots = task.getResult().getDocuments();
                    ArrayList<CommentDTO> comments = new ArrayList<>();
                    for(DocumentSnapshot doc : snapshots){
                        comments.add(doc.toObject(CommentDTO.class));
                    }
                    listener.onComplete(comments);
                    return;
                }
                listener.onComplete(null);
            }
        });
    }

    public void getPostComments(final String postId, final IGlobalListener<ArrayList<CommentDTO>> listener){
        getAllComments(new IGlobalListener<ArrayList<CommentDTO>>() {
            @Override
            public void onComplete(ArrayList<CommentDTO> comments) {
                if(comments != null){
                    ArrayList<CommentDTO> commentDTOList = new ArrayList<>();
                    for(CommentDTO commentDTO : comments){
                        if(commentDTO != null && commentDTO.getPostId() != null && commentDTO.getPostId().equals(postId))
                            commentDTOList.add(commentDTO);
                    }
                    listener.onComplete(commentDTOList);
                    return;
                }
                listener.onComplete(null);
            }
        });
    }

    public void likePost(final String userId, final String postId, final IGlobalListener<Boolean> listener){
        getPostById(postId, new IGlobalListener<PostDTO>() {
            @Override
            public void onComplete(PostDTO post) {
                if(post != null){
                    ArrayList<String> postLikes = post.getLikes();
                    if(postLikes != null)
                        if(postLikes.contains(userId)){
                            postLikes.remove(userId);
                            Map<String,Object> newLikesMap = new HashMap<>();
                            newLikesMap.put(likesField,postLikes);
                            newLikesMap.put(numberOfLikesField,postLikes.size());
                            dataBase.collection(postsTable).document(postId).update(newLikesMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        listener.onComplete(true);
                                        return;
                                    }
                                    listener.onComplete(false);
                                }
                            });
                        }
                        else{
                            postLikes.add(userId);
                            Map<String,Object> newLikesMap = new HashMap<>();
                            newLikesMap.put(likesField,postLikes);
                            newLikesMap.put(numberOfLikesField,postLikes.size());
                            dataBase.collection(postsTable).document(postId).update(newLikesMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        listener.onComplete(true);
                                        return;
                                    }
                                    listener.onComplete(false);
                                }
                            });
                        }
                }
            }
        });
    }

    public void commentOnPost(String userId, String postId, final CommentDTO commentDTO, final IGlobalListener<Boolean> listener) {
        getPostById(postId, new IGlobalListener<PostDTO>() {
            @Override
            public void onComplete(PostDTO post) {
                if(post != null){
                    dataBase.collection(commentsTable).document(commentDTO.getCommentId()).set(commentDTO).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                listener.onComplete(true);
                                return;
                            }
                            listener.onComplete(false);
                        }
                    });
                }
            }
        });
    }

    public void followThisProfile(final String userId, final IGlobalListener<String> listener) {
        getUserById(userId, new IGetUserListener() {
            @Override
            public void onComplete(final UserDTO givenUserDTO) throws MyException, NoSuchAlgorithmException {
                if(givenUserDTO != null){
                    final String currentUserId = getFireBaseAuthInstance().getCurrentUser().getUid();
                    ArrayList<String> givenUserfollowers = givenUserDTO.getFollowers();
                    final Map<String,Object> otherUserHashMap = new HashMap<>();
                    final Map<String,Object> currentUserHashMap = new HashMap<>();

                    if(givenUserfollowers == null)
                        givenUserfollowers = new ArrayList<>();
                    if(givenUserfollowers.contains(currentUserId)){
                        final ArrayList<String> finalGivenUserfollowers = givenUserfollowers;
                        getUserById(currentUserId, new IGetUserListener() {
                            @Override
                            public void onComplete(UserDTO currentUserDTO) throws MyException, NoSuchAlgorithmException {
                                if(currentUserDTO != null){
                                    if(currentUserDTO.getFollowing() == null)
                                        currentUserDTO.setFollowing(new ArrayList<String>());
                                    ArrayList<String> currentUserfollowing = currentUserDTO.getFollowing();
                                    if(currentUserfollowing.contains(userId)){
                                        currentUserfollowing.remove(userId);
                                        currentUserDTO.setFollowing(currentUserfollowing);
                                    }
                                    finalGivenUserfollowers.remove(currentUserId);
                                    givenUserDTO.setFollowers(finalGivenUserfollowers);
                                    otherUserHashMap.clear();
                                    currentUserHashMap.clear();
                                    otherUserHashMap.put(followersField, finalGivenUserfollowers);
                                    currentUserHashMap.put(followingField, currentUserfollowing);
                                    dataBase.collection(usersTable).document(String.valueOf(userId)).update(otherUserHashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            dataBase.collection(usersTable).document(String.valueOf(currentUserId)).update(currentUserHashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(task.isSuccessful()){
                                                        listener.onComplete(OtherProfileActivity.unfollowed);
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    }
                    else{
                        final ArrayList<String> finalGivenUserfollowers = givenUserfollowers;
                        getUserById(currentUserId, new IGetUserListener() {
                            @Override
                            public void onComplete(UserDTO currentUserDTO) throws MyException, NoSuchAlgorithmException {
                                if(currentUserDTO != null){
                                    ArrayList<String> currentUserDTOFollowing = currentUserDTO.getFollowing();
                                    if(currentUserDTOFollowing == null)
                                        currentUserDTOFollowing = new ArrayList<>();
                                    if(!currentUserDTOFollowing.contains(userId)){
                                        currentUserDTOFollowing.add(userId);
                                        finalGivenUserfollowers.add(currentUserId);
                                        otherUserHashMap.clear();
                                        currentUserHashMap.clear();
                                        otherUserHashMap.put(followersField,finalGivenUserfollowers);
                                        currentUserHashMap.put(followingField,currentUserDTOFollowing);
                                        dataBase.collection(usersTable).document(userId).update(otherUserHashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    dataBase.collection(usersTable).document(String.valueOf(currentUserId)).update(currentUserHashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful()){
                                                                listener.onComplete(OtherProfileActivity.followed);
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    public void getUserHomePosts(final String userId, final IGlobalListener<ArrayList<PostDTO>> listener) {

        final ArrayList<PostDTO> retPostDTOList = new ArrayList<>();

        getUserFollowing(userId, new IGlobalListener<Set<UserDTO>>() {
            @Override
            public void onComplete(Set<UserDTO> following) {
                if(following != null){
                    final ArrayList<String> userIdList = new ArrayList<>();
                    for(UserDTO user : following){
                        if(user != null)
                            userIdList.add(user.getId());
                    }
                    getAllPosts(new IGlobalListener<ArrayList<PostDTO>>() {
                        @Override
                        public void onComplete(ArrayList<PostDTO> posts) {
                            if(posts != null){
                                for(PostDTO post : posts){
                                    if(userIdList.contains(post.getUserId()))
                                        retPostDTOList.add(post);
                                }
                                getUserPosts(userId, new IGlobalListener<Set<PostDTO>>() {
                                    @Override
                                    public void onComplete(Set<PostDTO> posts) {
                                        for(PostDTO post : posts){
                                            long longDiff = new Date().getTime() - post.getDateOfPost().getTime();
                                            long days = TimeUnit.DAYS.convert(longDiff,TimeUnit.MILLISECONDS);
                                            if(days <= 2){
                                                retPostDTOList.add(post);
                                            }
                                        }
                                        listener.onComplete(retPostDTOList);
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    public void deletePost(String postId, final IGlobalListener<Boolean> listener) {
        dataBase.collection(postsTable).document(postId).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    listener.onComplete(true);
                    return;
                }
                listener.onComplete(false);
            }
        });
    }

    public void createNewBusiness(String userId, final BusinessDTO businessDTO, final Uri mImageUri, final IGlobalListener<BusinessDTO> listener) {
        getUserById(userId, new IGetUserListener() {
            @Override
            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    if(userDTO.getBusinessId() == null){
                        businessDTO.setBusinessId(StringsGenerator.GenerateNewUUID());
                        if(mImageUri == null){
                            getDefaultBusinessImage(new IGlobalListener<String>() {
                                @Override
                                public void onComplete(String object) {
                                    if(object != null){
                                        businessDTO.setBusinessLogo(object);
                                        dataBase.collection(businessesTable).document(businessDTO.getBusinessId()).set(businessDTO).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    getUserById(businessDTO.getBusinessUserId(), new IGetUserListener() {
                                                        @Override
                                                        public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                                                            if(userDTO != null) {
                                                                updateUserBusinessId(businessDTO.getBusinessUserId(), businessDTO.getBusinessId(), new IGlobalListener<Boolean>() {
                                                                    @Override
                                                                    public void onComplete(Boolean object) {
                                                                        if (object) {
                                                                            listener.onComplete(businessDTO);
                                                                            return;
                                                                        }
                                                                    }
                                                                });
                                                                return;
                                                            }
                                                            Toast.makeText(MyApplication.getContext(),"Any Problem with creating a new business.(createNewBusiness 1)",Toast.LENGTH_LONG).show();
                                                        }
                                                    });

                                                }
                                                else {
                                                    Toast.makeText(MyApplication.getContext(), "Any Problem with creating a new business. (createNewBusiness 2)", Toast.LENGTH_LONG).show();
                                                    listener.onComplete(null);
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                        else{
                            dataBase.collection(businessesTable).document(businessDTO.getBusinessId()).set(businessDTO).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        getUserById(businessDTO.getBusinessUserId(), new IGetUserListener() {
                                            @Override
                                            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                                                if(userDTO != null) {
                                                    updateUserBusinessId(businessDTO.getBusinessUserId(), businessDTO.getBusinessId(), new IGlobalListener<Boolean>() {
                                                        @Override
                                                        public void onComplete(Boolean object) {
                                                            if (object) {
                                                                if(businessDTO!=null && businessDTO.getBusinessId() != null){
                                                                    uploadBusinessLOGO(businessDTO.getBusinessId(), "business_" + businessDTO.getBusinessId(), mImageUri, "jpg", new IGlobalListener<Boolean>() {
                                                                        @Override
                                                                        public void onComplete(Boolean object) {
                                                                            if(object){
                                                                                listener.onComplete(businessDTO);
                                                                                return;
                                                                            }
                                                                            listener.onComplete(null);
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    });
                                                    return;
                                                }
                                                Toast.makeText(MyApplication.getContext(),"Any Problem with creating a new business.(createNewBusiness 1)",Toast.LENGTH_LONG).show();
                                            }
                                        });

                                    }
                                    else {
                                        Toast.makeText(MyApplication.getContext(), "Any Problem with creating a new business. (createNewBusiness 2)", Toast.LENGTH_LONG).show();
                                        listener.onComplete(null);
                                    }
                                }
                            });

                        }
                    }
                }
            }
        });
    }

    private void updateUserBusinessId(final String userId, final String businessId, final IGlobalListener<Boolean> listener) {
        getUserById(userId, new IGetUserListener() {
            @Override
            public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                if(userDTO != null){
                    DocumentReference documentReference = dataBase.collection(usersTable).document(userId);
                    HashMap<String,Object> hashMap = new HashMap();
                    hashMap.put(businessIdField,businessId);
                    documentReference.update(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                listener.onComplete(true);
                                return;
                            }
                            listener.onComplete(false);
                        }
                    });
                }
            }
        });
    }

    private void getAllBusnisses(final IGlobalListener<ArrayList<BusinessDTO>> listener) {
        dataBase.collection(businessesTable).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    List<DocumentSnapshot> snapshots = task.getResult().getDocuments();
                    ArrayList<BusinessDTO> comments = new ArrayList<>();
                    for(DocumentSnapshot doc : snapshots){
                        comments.add(doc.toObject(BusinessDTO.class));
                    }
                    listener.onComplete(comments);
                    return;
                }
                listener.onComplete(null);
            }
        });
    }

    public void getBusinessById(final String businessId, final IGlobalListener<BusinessDTO> listener) {
        getAllBusnisses(new IGlobalListener<ArrayList<BusinessDTO>>() {
            @Override
            public void onComplete(ArrayList<BusinessDTO> businesses) {
                for(BusinessDTO businessDTO : businesses){
                    if(businessDTO != null && businessDTO.getBusinessId().equals(businessId)) {
                        listener.onComplete(businessDTO);
                        return;
                    }
                }
                listener.onComplete(null);
            }
        });
    }

    public void getAllBusinessTypes(final IGlobalListener<HashMap<EBusinessType,String>> listener){
        dataBase.collection(businessTypesTable).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    HashMap<EBusinessType,String> retMap = new HashMap<>();
                    QuerySnapshot result = task.getResult();
                    for(DocumentSnapshot document : result.getDocuments()){
                        retMap.put(EBusinessType.values()[Integer.parseInt(document.getId())-1],(String)document.get("EBusinessType"));
                    }
                    listener.onComplete(retMap);
                }
            }
        });
    }

    public void getAllBusinessPayments(final IGlobalListener<HashMap<EBusinessPayment, String>> listener) {
        dataBase.collection(businessPaymentsTable).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    HashMap<EBusinessPayment,String> retMap = new HashMap<>();
                    QuerySnapshot result = task.getResult();
                    for(DocumentSnapshot document : result.getDocuments()){
                        retMap.put(EBusinessPayment.values()[Integer.parseInt(document.getId())-1],(String)document.get("EBusinessPayment"));
                    }
                    listener.onComplete(retMap);
                }
            }
        });
    }

    public void updateBusiness(final BusinessDTO businessDTO, final Uri mImageUri, final IGlobalListener<Boolean> listener) {
        getAllBusnisses(new IGlobalListener<ArrayList<BusinessDTO>>() {
            @Override
            public void onComplete(ArrayList<BusinessDTO> businesses) {
                if(businesses != null){
                    for(BusinessDTO business : businesses){
                        if(business != null && business.getBusinessId().equals(businessDTO.getBusinessId())){
                            DocumentReference documentReference = dataBase.collection(businessesTable).document(businessDTO.getBusinessId());
                            HashMap<String,Object> hashMap = new HashMap();
                            hashMap.put(businessNameField,businessDTO.getBusinessName());
                            hashMap.put(businessTypeField, BusinessHandler.getBusinessTypeEnumName(businessDTO.getBusinessType()));
                            hashMap.put(businessPaymentField,BusinessHandler.getBusinessPaymentEnumName(businessDTO.getBusinessPayment()));
                            hashMap.put(businessAboutUsField, businessDTO.getBusinessAboutUs());
                            documentReference.update(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        uploadBusinessLOGO(businessDTO.getBusinessId(), "business_" + businessDTO.getBusinessId(), mImageUri, "jpg", new IGlobalListener<Boolean>() {
                                            @Override
                                            public void onComplete(Boolean object) {
                                                if(object){
                                                    listener.onComplete(true);
                                                    return;
                                                }
                                                listener.onComplete(false);
                                            }
                                        });
                                    }
                                    listener.onComplete(false);
                                }
                            });
                        }
                    }
                }
            }
        });
    }

    public void uploadBusinessLOGO(final String businessId, String logoName, Uri mImageUri, String extension, final IGlobalListener<Boolean> listener) {

        final StorageReference fileReference = storageReference.child("Businesses/"+logoName+"."+extension);
        UploadTask uploadTask = fileReference.putFile(mImageUri);

        uploadTask.continueWithTask(new Continuation() {
            @Override
            public Object then(@NonNull Task task) throws Exception {
                if (!task.isSuccessful()) {
                    throw  task.getException();
                }
                return fileReference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if(task.isSuccessful()){
                    DocumentReference documentReference = FirebaseFirestore.getInstance().collection(businessesTable).document(businessId);
                    documentReference.update(businessLogoField,task.getResult().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful())
                                listener.onComplete(true);
                            else
                                listener.onComplete(false);
                        }
                    });
                    return;
                }
                listener.onComplete(false);
            }
        });
    }


}






















