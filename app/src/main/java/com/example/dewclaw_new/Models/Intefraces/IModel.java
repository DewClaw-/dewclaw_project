package com.example.dewclaw_new.Models.Intefraces;

import android.net.Uri;

import com.example.dewclaw_new.DTOs.BusinessDTO;
import com.example.dewclaw_new.DTOs.CommentDTO;
import com.example.dewclaw_new.DTOs.PostDTO;
import com.example.dewclaw_new.DTOs.SearchContentDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EBusinessPayment;
import com.example.dewclaw_new.Enums.EBusinessType;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.google.android.gms.tasks.OnCompleteListener;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public interface IModel {
    void logUserOut();

    void createNewUser(UserDTO userDTO,OnCompleteListener listener) throws MyException, NoSuchAlgorithmException;

    void getUserById(String idToCheck, IGetUserListener listener) throws NoSuchAlgorithmException, MyException;

    void logUserIn(String email, String password, OnCompleteListener listener) throws MyException;

    void uploadImage(String imageToString, Uri path, String postText, String extension, IGlobalListener<Boolean> listener) throws NoSuchAlgorithmException;

    void getAllUsers(final IGlobalListener<ArrayList<UserDTO>> listener);

    void getAllExceptions(final IGetAllExceptionsListener listener);

    void getUserProfilePicture(String userId, final IGlobalListener<String> listener);

    void uploadProfilePictureImage(final String name ,Uri uri , String extension, final IGlobalListener<Boolean> listener);

    void updateUserDetails(final UserDTO userDTO, final IGlobalListener<UserDTO> listener);

    void searchContent(final String inputText, final IGlobalListener<Set<SearchContentDTO>> listener);

    void getUserFollowing(String userId, final IGlobalListener<Set<UserDTO>> listener);

    void getUserFollowers(String userId, final IGlobalListener<Set<UserDTO>> listener);

    void getAllPosts(final IGlobalListener<ArrayList<PostDTO>> listener);

    void getUserPosts(final String userId , final IGlobalListener<Set<PostDTO>> listener);

    void getPostById(final String postId, final IGlobalListener<PostDTO> listener);

    void getAllComments(final IGlobalListener<ArrayList<CommentDTO>> listener);

    void getPostComments(final String postId, final IGlobalListener<ArrayList<CommentDTO>> listener);

    void likePost(final String userId, final String postId, final IGlobalListener<Boolean> listener);

    void commentOnPost(String userId, String postId, final CommentDTO commentDTO, final IGlobalListener<Boolean> listener);

    void followThisProfile(final String userId, final IGlobalListener<String> listener);

    void getUserHomePosts(final String userId, final IGlobalListener<ArrayList<PostDTO>> listener);

    void deletePost(String postId, final IGlobalListener<Boolean> listener);

    void createNewBusiness(String userId, final BusinessDTO businessDTO, final Uri mImageUri, final IGlobalListener<BusinessDTO> listener);

    void getBusinessById(final String businessId, final IGlobalListener<BusinessDTO> listener);

    void getAllBusinessTypes(final IGlobalListener<HashMap<EBusinessType,String>> listener);

    void getAllBusinessPayments(final IGlobalListener<HashMap<EBusinessPayment, String>> listener);

    void updateBusiness(final BusinessDTO businessDTO, final Uri mImageUri, final IGlobalListener<Boolean> listener);

    void uploadBusinessLOGO(final String businessId, String logoName, Uri mImageUri, String extension, final IGlobalListener<Boolean> listener);
}
