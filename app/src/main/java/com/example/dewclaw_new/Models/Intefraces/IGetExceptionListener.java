package com.example.dewclaw_new.Models.Intefraces;

import com.example.dewclaw_new.DTOs.ExceptionDTO;

public interface IGetExceptionListener {
    void onComplete(ExceptionDTO exceptionDTO);
}
