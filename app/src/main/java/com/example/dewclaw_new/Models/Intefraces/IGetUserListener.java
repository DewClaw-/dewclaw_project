package com.example.dewclaw_new.Models.Intefraces;

import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Exceptions.MyException;

import java.security.NoSuchAlgorithmException;

public interface IGetUserListener {

    void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException;
}
