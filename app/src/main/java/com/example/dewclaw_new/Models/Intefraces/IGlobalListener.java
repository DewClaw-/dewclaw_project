package com.example.dewclaw_new.Models.Intefraces;

public interface IGlobalListener<T> {
    void onComplete(T object);
}
