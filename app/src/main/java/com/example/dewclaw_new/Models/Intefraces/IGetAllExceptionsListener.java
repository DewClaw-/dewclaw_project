package com.example.dewclaw_new.Models.Intefraces;

import com.example.dewclaw_new.Exceptions.EExceptions;

import java.util.HashMap;

public interface IGetAllExceptionsListener {

    void onComplete(HashMap<EExceptions,String> map);
}
