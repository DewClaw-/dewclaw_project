package com.example.dewclaw_new.BusinessUtils;

import com.example.dewclaw_new.Enums.EBusinessPayment;
import com.example.dewclaw_new.Enums.EBusinessType;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.EExceptions;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGetAllExceptionsListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;

import java.util.ArrayList;
import java.util.HashMap;

public class BusinessUtils {

    private static HashMap<EBusinessType,String> businessTypeMap = new HashMap<>();
    private static HashMap<EBusinessPayment,String> businessPaymentMap = new HashMap<>();

    private static void init(final IGlobalListener<Boolean> listener){

        final FireBaseModel fireBaseModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);

        fireBaseModel.getAllBusinessTypes(new IGlobalListener<HashMap<EBusinessType,String>>() {

            @Override
            public void onComplete(HashMap<EBusinessType, String> typesMap) {
                if(typesMap != null){
                    businessTypeMap = typesMap;
                    fireBaseModel.getAllBusinessPayments(new IGlobalListener<HashMap<EBusinessPayment, String>>() {
                        @Override
                        public void onComplete(HashMap<EBusinessPayment, String> paymentsMap) {
                            if(paymentsMap != null){
                                businessPaymentMap = paymentsMap;
                                listener.onComplete(true);
                            }
                            else{
                                listener.onComplete(false);
                            }
                        }
                    });
                }
                else{
                    listener.onComplete(false);
                }
            }
        });
    }

    public static void getAllBusinessTypes(final IGlobalListener<ArrayList<String>> listener){
        if(businessTypeMap.isEmpty()){
            init(new IGlobalListener<Boolean>() {
                @Override
                public void onComplete(Boolean object) {
                    if(object){
                        listener.onComplete(new ArrayList<>(businessTypeMap.values()));
                    }
                }
            });
        }
        else{
            listener.onComplete(new ArrayList<>(businessTypeMap.values()));
        }
    }

    public static void getAllBusinessPayments(final IGlobalListener<ArrayList<String>> listener){
        if(businessPaymentMap.isEmpty()){
            init(new IGlobalListener<Boolean>() {
                @Override
                public void onComplete(Boolean object) {
                    if(object){
                        listener.onComplete(new ArrayList<String>(businessPaymentMap.values()));
                    }
                }
            });
        }
        else{
            listener.onComplete(new ArrayList<>(businessPaymentMap.values()));
        }
    }
}
