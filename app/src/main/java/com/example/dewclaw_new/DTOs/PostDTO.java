package com.example.dewclaw_new.DTOs;

import java.util.ArrayList;
import java.util.Date;

public class PostDTO {

    String PostId;
    String UserId;
    String PostText;
    String PostImage;
    Date DateOfPost;
    ArrayList<String> Likes;
    Integer numberOfLikes;


    public PostDTO(){}

    public String getPostId() {
        return PostId;
    }

    public void setPostId(String postId) {
        PostId = postId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getPostText() {
        return PostText;
    }

    public void setPostText(String postText) {
        PostText = postText;
    }

    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public Date getDateOfPost() {
        return DateOfPost;
    }

    public void setDateOfPost(Date dateOfPost) {
        DateOfPost = dateOfPost;
    }

    public ArrayList<String> getLikes() {
        return Likes;
    }

    public void setLikes(ArrayList<String> likes) {
        Likes = likes;
    }

    public Integer getNumberOfLikes() {
        return numberOfLikes;
    }

    public void setNumberOfLikes(Integer numberOfLikes) {
        this.numberOfLikes = numberOfLikes;
    }
}
