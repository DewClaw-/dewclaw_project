package com.example.dewclaw_new.DTOs;

import androidx.annotation.Nullable;

public class SearchContentDTO {

    UserDTO userDTO;
    BusinessDTO businessDTO;



    public SearchContentDTO(){}

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public BusinessDTO getBusinessDTO() {
        return businessDTO;
    }

    public void setBusinessDTO(BusinessDTO businessDTO) {
        this.businessDTO = businessDTO;
    }

    @Override
    public int hashCode() {
        if(userDTO != null)
            return userDTO.getId().hashCode();
        if(businessDTO != null)
            return businessDTO.getBusinessId().hashCode();
        return super.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(userDTO != null && obj != null){
            SearchContentDTO searchContentDTO = (SearchContentDTO)obj;
            return userDTO.equals(searchContentDTO.getUserDTO());
        }
        if(businessDTO != null && obj != null){
            SearchContentDTO searchContentDTO = (SearchContentDTO) obj;
            return businessDTO.equals(searchContentDTO.getBusinessDTO());
        }

        return super.equals(obj);
    }
}
