package com.example.dewclaw_new.DTOs;

import com.example.dewclaw_new.Enums.EBusinessPayment;
import com.example.dewclaw_new.Enums.EBusinessType;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class BusinessDTO {

    String BusinessId;
    String BusinessUserId;
    String BusinessName;
    EBusinessType BusinessType;
    EBusinessPayment BusinessPayment;
    String BusinessAboutUs;
    String BusinessLogo;



    public BusinessDTO(){}

    public String getBusinessId() {
        return BusinessId;
    }

    public void setBusinessId(String businessId) {
        BusinessId = businessId;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public EBusinessType getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(EBusinessType businessType) {
        BusinessType = businessType;
    }

    public EBusinessPayment getBusinessPayment() {
        return BusinessPayment;
    }

    public void setBusinessPayment(EBusinessPayment businessPayment) {
        BusinessPayment = businessPayment;
    }

    public String getBusinessAboutUs() {
        return BusinessAboutUs;
    }

    public void setBusinessAboutUs(String businessAboutUs) {
        BusinessAboutUs = businessAboutUs;
    }

    public String getBusinessLogo() {
        return BusinessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        BusinessLogo = businessLogo;
    }

    public String getBusinessUserId() {
        return BusinessUserId;
    }

    public void setBusinessUserId(String businessUserId) {
        BusinessUserId = businessUserId;
    }

    @Override
    public boolean equals(@Nullable Object business) {
        if(business == null || !(business instanceof UserDTO))
            return false;
        if(business == this)
            return true;
        BusinessDTO givenBusiness = (BusinessDTO) business;
        return this.getBusinessId().equals(givenBusiness.getBusinessId());
    }

    @Override
    public int hashCode() {
        if(getBusinessId() != null)
            return getBusinessId().hashCode();
        return super.hashCode();
    }
}
