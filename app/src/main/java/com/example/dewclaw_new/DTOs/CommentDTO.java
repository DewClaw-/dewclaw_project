package com.example.dewclaw_new.DTOs;

import java.util.Date;

public class CommentDTO implements Comparable<CommentDTO>{

    String CommentId;
    String Text;
    String UserId;
    String PostId;
    Date CommentDate;

    public CommentDTO() {}

    public String getCommentId() {
        return CommentId;
    }

    public void setCommentId(String commentId) {
        CommentId = commentId;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getPostId() {
        return PostId;
    }

    public void setPostId(String postId) {
        PostId = postId;
    }

    public Date getCommentDate() {
        return CommentDate;
    }

    public void setCommentDate(Date commentDate) {
        CommentDate = commentDate;
    }


    @Override
    public int compareTo(CommentDTO o) {
        return getCommentDate().compareTo(o.getCommentDate());
    }
}
