package com.example.dewclaw_new.DTOs;

import com.example.dewclaw_new.Exceptions.EExceptions;

public class ExceptionDTO {
    EExceptions Id;
    String Text;

    public ExceptionDTO() { }

    public ExceptionDTO(EExceptions id, String text){
        Id = id;
        Text = text;
    }

    public EExceptions getId() {
        return Id;
    }

    public void setId(EExceptions id) {
        Id = id;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }
}
