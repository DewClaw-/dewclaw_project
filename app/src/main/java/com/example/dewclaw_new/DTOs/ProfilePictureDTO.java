package com.example.dewclaw_new.DTOs;

import android.net.Uri;

import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;

public class ProfilePictureDTO {

    public Integer DEFAULT_PROFILE_PICTURE;

    public String ExternalPicture;

    public Boolean NeedDefault;

    public ProfilePictureDTO() {
        String imagename = "default_profile_image";
        int res = MyApplication.getContext().getResources().getIdentifier(imagename, "drawable",  MyApplication.getContext().getPackageName());
        DEFAULT_PROFILE_PICTURE = res;
    }
}
