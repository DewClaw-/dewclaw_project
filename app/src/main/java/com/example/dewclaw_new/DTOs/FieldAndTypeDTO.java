package com.example.dewclaw_new.DTOs;

public class FieldAndTypeDTO {
    String fieldName;
    String type;
    boolean isKey;

    public FieldAndTypeDTO(String fieldName, String type, boolean isKey) {
        this.fieldName = fieldName;
        this.type = type;
        this.isKey = isKey;
    }

    public String get(){
        if(isKey)
            return fieldName+" "+type+" "+"NOT NULL PRIMARY KEY";
        return fieldName+" "+type+" NULL";
    }
}
