package com.example.dewclaw_new.DTOs;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class UserDTO implements Parcelable {

    @PrimaryKey
    private String Id;

    private String Email;

    private String Password;

    private String UserName;

    private String FirstName;

    private String LastName;

    private String ProfileImage;

    private String Age;

    private String DogName;

    private ArrayList<String> Following;

    private ArrayList<String> Followers;

    private String DogAge;

    private String DogBreed;

    private String FullName;

    private String BusinessId;



    public UserDTO(){}

    public UserDTO(JSONObject json) throws JSONException {
        Email = json.getString("email");
        Password = json.getString("password");
        UserName = json.getString("username");
    }

    public UserDTO(String email, String password, String userName) {
        Password = password;
        Email = email;
        UserName = userName;
        Id = "No_ID";
    }

    protected UserDTO(Parcel in) {
        Id = in.readString();
        Email = in.readString();
        Password = in.readString();
        UserName = in.readString();
        FirstName = in.readString();
        LastName = in.readString();
        ProfileImage = in.readString();
        Age = in.readString();
        DogName = in.readString();
        DogAge = in.readString();
        DogBreed = in.readString();
        FullName = in.readString();
    }

    public static final Creator<UserDTO> CREATOR = new Creator<UserDTO>() {
        @Override
        public UserDTO createFromParcel(Parcel in) {
            return new UserDTO(in);
        }

        @Override
        public UserDTO[] newArray(int size) {
            return new UserDTO[size];
        }
    };

    public void setEmail(String email) {
        Email = email;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getEmail() {
        return Email;
    }

    public String getUserName() {
        return UserName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public ArrayList<String> getFollowing() {
        return Following;
    }

    public void setFollowing(ArrayList<String> following) {
        Following = following;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getDogName() {
        return DogName;
    }

    public void setDogName(String dogName) {
        DogName = dogName;
    }

    public String getDogAge() {
        return DogAge;
    }

    public void setDogAge(String dogAge) {
        DogAge = dogAge;
    }

    public String getDogBreed() {
        return DogBreed;
    }

    public void setDogBreed(String dogBreed) {
        DogBreed = dogBreed;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public ArrayList<String> getFollowers() {
        return Followers;
    }

    public void setFollowers(ArrayList<String> followers) {
        Followers = followers;
    }

    public String getBusinessId() {
        return BusinessId;
    }

    public void setBusinessId(String businessId) {
        BusinessId = businessId;
    }


    @Override
    public boolean equals(@Nullable Object user) {
        if(user == null || !(user instanceof UserDTO))
            return false;
        if(user == this)
            return true;
        UserDTO givenUser = (UserDTO) user;
        return this.getId().equals(givenUser.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Id);
        dest.writeString(Email);
        dest.writeString(Password);
        dest.writeString(UserName);
        dest.writeString(FirstName);
        dest.writeString(LastName);
        dest.writeString(ProfileImage);
        dest.writeString(Age);
        dest.writeString(DogName);
        dest.writeString(DogAge);
        dest.writeString(DogBreed);
        dest.writeString(FullName);
    }
}
