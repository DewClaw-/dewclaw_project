package com.example.dewclaw_new;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyHandler {

    private static VolleyHandler volleyHandler;
    private Context context;
    private RequestQueue queue;


    private VolleyHandler(Context context) {
        this.context = context;
        queue = getRequestQueue();
    }

    public static synchronized VolleyHandler getInstance(Context context){
        if(volleyHandler == null)
            volleyHandler = new VolleyHandler(context);

        return volleyHandler;
    }

    public RequestQueue getRequestQueue(){

        if(queue == null){
            queue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return queue;
    }

    public <T> void addRequestToQueue(Request<T> req){

        getRequestQueue().add(req);
    }
}
