package com.example.dewclaw_new.Enums;

import java.util.ArrayList;
import java.util.Date;

public enum EPostsFields {
    postId,
    userId,
    postText,
    postImage,
    dateOfPost,
    likes,
    numberOfLikes,
}


