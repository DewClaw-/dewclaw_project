package com.example.dewclaw_new.Enums;

import java.util.ArrayList;

public enum EUsersFields {
    id,
    email,
    password,
    userName,
    firstName,
    lastName,
    profileImage,
    age,
    dogName,
    following,
    followers,
    dogAge,
    dogBreed,
    fullName,
    businessId
}
