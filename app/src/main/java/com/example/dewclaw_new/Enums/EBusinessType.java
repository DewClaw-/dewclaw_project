package com.example.dewclaw_new.Enums;

public enum EBusinessType {
    Training,
    Adoption,
    Selling,
    DogWalking,
    Barbershop,
    BoardingHouse,
    Else
}
