package com.example.dewclaw_new.Enums;

import java.util.Date;

public enum ECommentsFields {
    commentId,
    text,
    userId,
    postId,
    commentDate
}
