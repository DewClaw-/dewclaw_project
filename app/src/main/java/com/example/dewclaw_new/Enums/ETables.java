package com.example.dewclaw_new.Enums;

public enum ETables{
    Users,
    Exceptions,
    Posts,
    Comments,
    Businesses,
    BusinessTypes,
    BusinessPayment
}

