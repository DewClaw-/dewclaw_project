package com.example.dewclaw_new.Enums;

public enum EBusinessesFields {
    businessId,
    businessUserId,
    businessName,
    businessType,
    businessPayment,
    businessAboutUs,
    businessLogo
}
