package com.example.dewclaw_new.Helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.dewclaw_new.DTOs.FieldAndTypeDTO;
import com.example.dewclaw_new.Enums.EBusinessesFields;
import com.example.dewclaw_new.Enums.ECommentsFields;
import com.example.dewclaw_new.Enums.EExceptionsFields;
import com.example.dewclaw_new.Enums.EPostsFields;
import com.example.dewclaw_new.Enums.ESqlTypes;
import com.example.dewclaw_new.Enums.ETables;
import com.example.dewclaw_new.Enums.EUsersFields;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class MySqliteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION =1;

    String listOfTables[] = {ETables.Users.name(),ETables.Posts.name(),ETables.Comments.name(),ETables.Businesses.name(),ETables.BusinessPayment.name(),ETables.BusinessTypes.name(),ETables.Exceptions.name()};

    FieldAndTypeDTO usersFieldArr[] = {new FieldAndTypeDTO(EUsersFields.id.name(), ESqlTypes.text.name(),true), new FieldAndTypeDTO(EUsersFields.email.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EUsersFields.password.name(),ESqlTypes.text.name(),false),
            new FieldAndTypeDTO(EUsersFields.userName.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EUsersFields.firstName.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EUsersFields.lastName.name(),ESqlTypes.text.name(),false),
            new FieldAndTypeDTO(EUsersFields.profileImage.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EUsersFields.age.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EUsersFields.dogName.name(),ESqlTypes.text.name(),false),
            new FieldAndTypeDTO(EUsersFields.following.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EUsersFields.followers.name(),ESqlTypes.text.name(),false),
            new FieldAndTypeDTO(EUsersFields.dogAge.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EUsersFields.dogBreed.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EUsersFields.fullName.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EUsersFields.businessId.name(),ESqlTypes.text.name(),false)};

    FieldAndTypeDTO postsFieldArr[] = {new FieldAndTypeDTO(EPostsFields.postId.name(),ESqlTypes.text.name(),true), new FieldAndTypeDTO(EPostsFields.userId.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EPostsFields.postText.name(),ESqlTypes.text.name(),false),
            new FieldAndTypeDTO(EPostsFields.postImage.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EPostsFields.dateOfPost.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EPostsFields.likes.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EPostsFields.numberOfLikes.name(),ESqlTypes.integer.name(),false)};

    FieldAndTypeDTO commentsFieldArr[] = {new FieldAndTypeDTO(ECommentsFields.commentId.name(),ESqlTypes.text.name(),true), new FieldAndTypeDTO(ECommentsFields.text.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(ECommentsFields.userId.name(),ESqlTypes.text.name(),false),
            new FieldAndTypeDTO(ECommentsFields.postId.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(ECommentsFields.commentDate.name(),ESqlTypes.text.name(),false)};

    FieldAndTypeDTO businessesFieldArr[] = {new FieldAndTypeDTO(EBusinessesFields.businessId.name(),ESqlTypes.text.name(),true), new FieldAndTypeDTO(EBusinessesFields.businessUserId.name(),ESqlTypes.text.name(),false),new FieldAndTypeDTO(EBusinessesFields.businessName.name(),ESqlTypes.text.name(),false),
            new FieldAndTypeDTO(EBusinessesFields.businessType.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EBusinessesFields.businessPayment.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EBusinessesFields.businessAboutUs.name(),ESqlTypes.text.name(),false), new FieldAndTypeDTO(EBusinessesFields.businessLogo.name(),ESqlTypes.text.name(),false)};

    FieldAndTypeDTO businessesPaymentFieldArr[] = {new FieldAndTypeDTO("EBusinessPayment",ESqlTypes.text.name(),true)};

    FieldAndTypeDTO businessesTypeFieldArr[] = {new FieldAndTypeDTO("EBusinessType",ESqlTypes.text.name(),true)};

    FieldAndTypeDTO exceptionsFieldArr[] = {new FieldAndTypeDTO(EExceptionsFields.id.name(),ESqlTypes.text.name(),true), new FieldAndTypeDTO(EExceptionsFields.text.name(),ESqlTypes.text.name(),false)};

    ArrayList<FieldAndTypeDTO[]> wholeDataBase = new ArrayList<>();

    public MySqliteHelper(@Nullable Context context) {
        super(context, "MyDataModel.db", null, DATABASE_VERSION);

        wholeDataBase.add(usersFieldArr);
        wholeDataBase.add(postsFieldArr);
        wholeDataBase.add(commentsFieldArr);
        wholeDataBase.add(businessesFieldArr);
        wholeDataBase.add(businessesPaymentFieldArr);
        wholeDataBase.add(businessesTypeFieldArr);
        wholeDataBase.add(exceptionsFieldArr);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(generateDbTablesAndFields());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(wholeDataBase != null){
            for(int i=0;i<wholeDataBase.size();i++){
                db.execSQL("DROP TABLE IF EXISTS "+listOfTables[i]);
            }
        }
        onCreate(db);
    }

    private String generateDbTablesAndFields(){
        String retValue = "";
        for(int i=0;i<wholeDataBase.size();i++){
            retValue += "CREATE TABLE IF NOT EXISTS "+listOfTables[i]+" (";
            for(int j=0; j < wholeDataBase.get(i).length;j++){
                if(j == wholeDataBase.get(i).length-1)
                    retValue += wholeDataBase.get(i)[j].get()+" \n";
                else
                    retValue += wholeDataBase.get(i)[j].get()+", \n";
            }
            retValue+=" ); \n";
        }

        return retValue;
    }
}
