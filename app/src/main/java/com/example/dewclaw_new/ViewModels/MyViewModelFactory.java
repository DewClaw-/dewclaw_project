package com.example.dewclaw_new.ViewModels;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class MyViewModelFactory<P,T> implements ViewModelProvider.Factory {

    P param;
    Class<T> type;

    public MyViewModelFactory(P param, Class<T> type){
        this.param = param;
        this.type = type;
    }

    @NonNull
    @Override
    public <P extends ViewModel> P create(@NonNull Class<P> modelClass) {
            return (P) new PostViewModel(param);
    }
}
