package com.example.dewclaw_new.ViewModels;

import android.util.Log;

import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.ModelManager;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import javax.annotation.Nullable;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class PostViewModel<T> extends ViewModel {

    MutableLiveData<Integer> numOfLikes = new MutableLiveData<>();
    String postId;

    public PostViewModel(T postId){
        if(!(postId instanceof String))
            return;
        Log.d("TAG","PostViewModel - Firebase :: " + postId);
        FireBaseModel instance = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);
        this.postId = (String)postId;

        dealWithUpdates(instance);
    }

    private void dealWithUpdates(FireBaseModel instance) {
        final FirebaseFirestore firebaseFirestore = instance.getDataBase();
        firebaseFirestore.collection("Posts").document(postId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if(e != null){
                    return;
                }
                if(documentSnapshot != null && documentSnapshot.exists() && ((String)documentSnapshot.get("postId")).equals(postId)){
                    Long num = (Long) documentSnapshot.get("numberOfLikes");
                    PostViewModel.this.numOfLikes.setValue(num.intValue());
                }
            }
        });
    }

    public String getPostId() {
        return postId;
    }

    public LiveData<Integer> getNumOfLikes() {

        Log.d("TAG","getNumOfLikes");
        if(numOfLikes == null)
            numOfLikes = new MutableLiveData<>();
        return numOfLikes;
    }


}


