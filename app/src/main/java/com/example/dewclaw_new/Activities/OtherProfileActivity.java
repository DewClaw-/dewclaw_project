package com.example.dewclaw_new.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dewclaw_new.DTOs.PostDTO;
import com.example.dewclaw_new.DTOs.ProfilePictureDTO;
import com.example.dewclaw_new.DTOs.SearchContentDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Fragments.ProfileFragment;
import com.example.dewclaw_new.Fragments.SearchFragment;
import com.example.dewclaw_new.Handlers.ChangeFragmentHandler;
import com.example.dewclaw_new.Handlers.UserDetailsHandler;
import com.example.dewclaw_new.Handlers.UserFollowingAndFollowersHandler;
import com.example.dewclaw_new.Handlers.UserProfilePictureHandler;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.Intefraces.IModel;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class OtherProfileActivity extends AppCompatActivity {

    CircleImageView profile_iv;
    TextView posts_num_tv;
    TextView followers_num_tv;
    TextView following_num_tv;
    TextView follow_this_profile_tv;

    CircleImageView userMenuImageView;
    TextView userMenuFullNameView;
    TextView userMenuEmailView;

    RecyclerView recyclerView;

    FireBaseModel mModel;

    NavigationView mNavigationView;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mActionDrawerToggle;

    String userId;

    public static String followed = "followed";
    public static String unfollowed = "unfollowed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_profile);

        Intent intent = getIntent();
        userId = intent.getStringExtra("UserId");


        profile_iv = findViewById(R.id.other_profile_image);
        posts_num_tv = findViewById(R.id.other_profile_posts_num_tv);
        followers_num_tv = findViewById(R.id.other_profile_followers_num_tv);
        following_num_tv = findViewById(R.id.other_profile_following_num_tv);

        follow_this_profile_tv = findViewById(R.id.other_profile_follow_this_profile);

        recyclerView = findViewById(R.id.other_profile_post_recycler_iv);

        mNavigationView = findViewById(R.id.other_profile_nav_view);
        mDrawerLayout = findViewById(R.id.other_profile_drawer_layout);
        mActionDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open,R.string.close);
        mDrawerLayout.addDrawerListener(mActionDrawerToggle);
        mActionDrawerToggle.syncState();
        setSupportActionBar((Toolbar)findViewById(R.id.my_toolbar));
        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_menu);


        follow_this_profile_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FollowThisProfile(new IGlobalListener<String>() {
                    @Override
                    public void onComplete(String object) {
                        if(followed.equals(object)){
                            follow_this_profile_tv.setBackgroundResource(R.color.backgroundSecondary);
                            follow_this_profile_tv.setText("Unfollow");
                        }
                        else if(unfollowed.equals(object)){
                            follow_this_profile_tv.setBackgroundResource(R.color.colorPrimary);
                            follow_this_profile_tv.setText("Follow");
                        }
                        init();
                    }
                });
            }
        });

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                ((FrameLayout)findViewById(R.id.other_profile_lower_part)).removeAllViews();
                ChangeFragmentHandler.changeFragmentDisplay(OtherProfileActivity.this,null,R.id.other_profile_lower_part, mDrawerLayout, getSupportFragmentManager() ,menuItem.getItemId());
                return true;
            }
        });



        init();
    }

    @Override
    protected void onStart() {
        super.onStart();

        UserProfilePictureHandler.getUserProfilePicture(mModel.getFireBaseAuthInstance().getCurrentUser().getUid(), new IGlobalListener<ProfilePictureDTO>() {
            @Override
            public void onComplete(ProfilePictureDTO object) {
                if(object != null) {
                    userMenuImageView = findViewById(R.id.user_iv);
                    if(object.NeedDefault){
                        userMenuImageView.setImageResource(object.DEFAULT_PROFILE_PICTURE);
                    }
                    else{
                        Picasso.with(MyApplication.getContext()).load(object.ExternalPicture).into(userMenuImageView);
                    }
                }
            }
        });

        UserDetailsHandler.getUserDetails(new IGlobalListener<UserDTO>() {
            @Override
            public void onComplete(UserDTO object) {
                if(object != null){
                    userMenuFullNameView = findViewById(R.id.user_full_name);
                    userMenuEmailView = findViewById(R.id.user_menu_email);
                    String fullName = generateFullName(object.getFirstName(),object.getLastName());
                    userMenuFullNameView.setText(fullName);
                    userMenuEmailView.setText(object.getEmail());
                }
            }

            private String generateFullName(String firstName, String lastName) {
                if(firstName == null)
                    return "";
                if(firstName != null && lastName == null)
                    return firstName;
                if(firstName != null && lastName != null)
                    return firstName+" "+lastName;
                return "";
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(mActionDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main_menu,menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    private void init() {

        mModel = (FireBaseModel)ModelManager.getInstance(EModel.FireBase);

        UserFollowingAndFollowersHandler.getUserFollowers(userId, new IGlobalListener<ArrayList<UserDTO>>() {
            @Override
            public void onComplete(ArrayList<UserDTO> followers) {
                if(followers != null){
                    followers_num_tv.setText(""+followers.size());
                    for(UserDTO user : followers)
                    if(user.getId().equals(mModel.getFireBaseAuthInstance().getCurrentUser().getUid())){
                        follow_this_profile_tv.setBackgroundResource(R.color.backgroundSecondary);
                        follow_this_profile_tv.setText("Unfollow");
                    }
                }
            }
        });

        UserFollowingAndFollowersHandler.getUserFollowing(userId, new IGlobalListener<ArrayList<UserDTO>>() {
            @Override
            public void onComplete(ArrayList<UserDTO> following) {
                if(following != null){
                    following_num_tv.setText(""+following.size());
                }
            }
        });

        UserProfilePictureHandler.getUserProfilePicture(userId, new IGlobalListener<ProfilePictureDTO>() {
            @Override
            public void onComplete(ProfilePictureDTO object) {
                if(object != null){
                    if(object.NeedDefault){

                    }
                    else {
                        Picasso.with(MyApplication.getContext()).load(object.ExternalPicture).into(profile_iv);
                    }
                }
            }
        });

        mModel.getUserPosts(userId, new IGlobalListener<Set<PostDTO>>() {
            @Override
            public void onComplete(Set<PostDTO> object) {
                if(object != null){
                    recyclerView.setLayoutManager(new GridLayoutManager(MyApplication.getContext(),3));
                    recyclerView.setAdapter(new RVAdapterPosts(new ArrayList<PostDTO>(object),MyApplication.getContext()));
                    posts_num_tv.setText(""+object.size());
                }
            }
        });
    }

    private void FollowThisProfile(final IGlobalListener<String> listener) {
        mModel.followThisProfile(userId, new IGlobalListener<String>() {
            @Override
            public void onComplete(String object) {
                listener.onComplete(object);
            }
        });
    }

    public class RVAdapterPosts extends RecyclerView.Adapter<OtherProfileActivity.RVAdapterPosts.PostViewHolder>{

        Context mContext;
        List<PostDTO> postDTOList;

        public RVAdapterPosts(List<PostDTO> posts, Context context){
            postDTOList = posts;
            mContext = context;
        }

        @NonNull
        @Override
        public OtherProfileActivity.RVAdapterPosts.PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.other_profile_post_content, parent,false);
            OtherProfileActivity.RVAdapterPosts.PostViewHolder viewHolder = new OtherProfileActivity.RVAdapterPosts.PostViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull OtherProfileActivity.RVAdapterPosts.PostViewHolder holder, int position) {
            PostDTO postDTO = postDTOList.get(position);

            holder.setDetails(postDTO.getPostImage(),postDTO.getPostId());
        }

        @Override
        public int getItemCount() {
            if(postDTOList != null)
                return postDTOList.size();
            return 0;
        }

        public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            View view;
            String givenPostId;

            public PostViewHolder(@NonNull View itemView) {
                super(itemView);

                view = itemView;
            }

            public void setDetails(String imageFromStorage, final String postId){

                final ImageView image_iv = view.findViewById(R.id.other_profile_post_content_iv);
                Picasso.with(MyApplication.getContext()).load(imageFromStorage).into(image_iv);
                givenPostId = postId;

                image_iv.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyApplication.getContext(),PostPageActivity.class);
                intent.putExtra("PostId",givenPostId);
                intent.putExtra("UserId", userId);
                startActivity(intent);
            }
        }
    }
}
