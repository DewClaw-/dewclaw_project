package com.example.dewclaw_new.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dewclaw_new.DTOs.CommentDTO;
import com.example.dewclaw_new.DTOs.PostDTO;
import com.example.dewclaw_new.DTOs.ProfilePictureDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.EExceptions;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Generators.StringsGenerator;
import com.example.dewclaw_new.Handlers.ChangeFragmentHandler;
import com.example.dewclaw_new.Handlers.ErrorHandler;
import com.example.dewclaw_new.Handlers.UserDetailsHandler;
import com.example.dewclaw_new.Handlers.UserProfilePictureHandler;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGetUserListener;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.MyApplication;
import com.example.dewclaw_new.R;
import com.example.dewclaw_new.ViewModels.MyViewModelFactory;
import com.example.dewclaw_new.ViewModels.PostViewModel;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class PostPageActivity extends AppCompatActivity {

    ImageView post_page_iv;
    TextView post_text_tv;
    Button btn_like_post_page;
    TextView num_of_likes_post_page;
    EditText edit_text_comment_post_page;
    Button btn_comment_post_page;
    Button btn_delete_post_page;

    String globalPostId;
    String globalUserId;
    String globalCurrentUserId;

    FireBaseModel mModel;
    ModelManager modelManager;

    RecyclerView recyclerView;

    NavigationView mNavigationView;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mActionDrawerToggle;

    CircleImageView userMenuImageView;
    TextView userMenuFullNameView;
    TextView userMenuEmailView;

    PostViewModel postViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_page);

        globalPostId = getIntent().getStringExtra("PostId");
        globalUserId = getIntent().getStringExtra("UserId");

        modelManager = new ModelManager();
        mModel = (FireBaseModel)ModelManager.getInstance(EModel.FireBase);
        globalCurrentUserId = mModel.getFireBaseAuthInstance().getCurrentUser().getUid();

        post_page_iv = findViewById(R.id.post_page_iv);
        post_text_tv = findViewById(R.id.post_text_tv);
        btn_like_post_page = findViewById(R.id.btn_like_post_page);
        num_of_likes_post_page = findViewById(R.id.num_of_likes_post_page);
        edit_text_comment_post_page = findViewById(R.id.edit_text_comment_post_page);
        btn_comment_post_page = findViewById(R.id.btn_comment_post_page);
        btn_delete_post_page = findViewById(R.id.btn_delete_post_page);

        if(!globalCurrentUserId.equals(globalUserId)) {
            btn_delete_post_page.setEnabled(false);
            btn_delete_post_page.setVisibility(View.GONE);
        }


        recyclerView = findViewById(R.id.comments_rv_post_page);
        recyclerView.setNestedScrollingEnabled(false);


        mNavigationView = findViewById(R.id.post_page_nav_view);
        mDrawerLayout = findViewById(R.id.post_page_drawer_layout);
        mActionDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open,R.string.close);
        mDrawerLayout.addDrawerListener(mActionDrawerToggle);
        mActionDrawerToggle.syncState();
        setSupportActionBar((Toolbar)findViewById(R.id.my_toolbar));
        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_menu);


        postViewModel = ViewModelProviders.of(this, new MyViewModelFactory(globalPostId,String.class)).get(PostViewModel.class);
        postViewModel.getNumOfLikes().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer num) {
                Log.d("TAG","onChanged :: postViewModel");
                Integer value = (Integer) postViewModel.getNumOfLikes().getValue();
                num_of_likes_post_page.setText(""+value);
            }
        });

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                ((FrameLayout)findViewById(R.id.post_page_lower_part)).removeAllViews();
                ChangeFragmentHandler.changeFragmentDisplay(PostPageActivity.this,null,R.id.post_page_lower_part, mDrawerLayout, getSupportFragmentManager() ,menuItem.getItemId());
                return true;
            }
        });

        btn_like_post_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LikePost(new IGlobalListener<Boolean>() {
                    @Override
                    public void onComplete(Boolean object) {
                        if(object){
                            Integer value = (Integer) postViewModel.getNumOfLikes().getValue();
                            num_of_likes_post_page.setText(""+value);
                        }
                    }
                });

            }
        });


        btn_comment_post_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edit_text_comment_post_page.getText() == null || edit_text_comment_post_page.getText().toString().equals(""))
                    ErrorHandler.setError(edit_text_comment_post_page, EExceptions.EmptyFieldMessage);
                else{
                    CommentOnPost(new IGlobalListener<Boolean>() {
                        @Override
                        public void onComplete(Boolean object) {
                            if(object) {
                                init();
                                edit_text_comment_post_page.setText("");
                            }
                        }
                    });
                }
            }
        });

        btn_delete_post_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PostPageActivity.this);

                builder.setTitle("Attention!");
                builder.setMessage("Do you really want to delete this post?");

                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(PostPageActivity.this,"Deleting you post...",Toast.LENGTH_LONG).show();
                        DeletePost();
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.show();

            }
        });

        init();
    }



    private void init() {
        globalCurrentUserId = mModel.getFireBaseAuthInstance().getCurrentUser().getUid();


        mModel.getPostById(globalPostId,new IGlobalListener<PostDTO>() {
            @Override
            public void onComplete(PostDTO post) {
                if(post != null){
                    Picasso.with(MyApplication.getContext()).load(post.getPostImage()).into(post_page_iv);
                            post_text_tv.setText(""+post.getPostText());
                            num_of_likes_post_page.setText(""+ postViewModel.getNumOfLikes().getValue());
                            mModel.getUserFollowers(globalUserId, new IGlobalListener<Set<UserDTO>>() {
                                @Override
                                public void onComplete(Set<UserDTO> followers) {
                                    if (followers != null) {
                                        ArrayList<String> listOfUserId = new ArrayList<>();
                                        for (UserDTO follower : followers) {
                                            listOfUserId.add(follower.getId());
                                        }
                                        if (!listOfUserId.contains(globalCurrentUserId) && !globalUserId.equals(globalCurrentUserId)) {
                                            btn_comment_post_page.setEnabled(false);
                                            edit_text_comment_post_page.setEnabled(false);
                                        }
                                    }
                                    else{
                                        btn_comment_post_page.setEnabled(false);
                                        edit_text_comment_post_page.setEnabled(false);
                                    }
                                }
                            });
                            mModel.getPostComments(globalPostId, new IGlobalListener<ArrayList<CommentDTO>>() {
                                @RequiresApi(api = Build.VERSION_CODES.N)
                                @Override
                                public void onComplete(ArrayList<CommentDTO> comments) {
                                    if(comments != null){
                                        Collections.sort(comments);
                                        recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));
                                        recyclerView.setAdapter(new RVAdapterComments(comments,MyApplication.getContext()));
                                    }
                                }
                            });
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        UserProfilePictureHandler.getUserProfilePicture(globalUserId, new IGlobalListener<ProfilePictureDTO>() {
            @Override
            public void onComplete(ProfilePictureDTO object) {
                if(object != null) {
                    userMenuImageView = findViewById(R.id.user_iv);
                    if(object.NeedDefault){
                        userMenuImageView.setImageResource(object.DEFAULT_PROFILE_PICTURE);
                    }
                    else{
                        Picasso.with(MyApplication.getContext()).load(object.ExternalPicture).into(userMenuImageView);
                    }
                }
            }
        });

        UserDetailsHandler.getUserDetails(new IGlobalListener<UserDTO>() {
            @Override
            public void onComplete(UserDTO object) {
                if(object != null){
                    userMenuFullNameView = findViewById(R.id.user_full_name);
                    userMenuEmailView = findViewById(R.id.user_menu_email);
                    String fullName = generateFullName(object.getFirstName(),object.getLastName());
                    userMenuFullNameView.setText(fullName);
                    userMenuEmailView.setText(object.getEmail());
                }
            }

            private String generateFullName(String firstName, String lastName) {
                if(firstName == null)
                    return "";
                if(firstName != null && lastName == null)
                    return firstName;
                if(firstName != null && lastName != null)
                    return firstName+" "+lastName;
                return "";
            }
        });
    }

    private void LikePost(final IGlobalListener<Boolean> listener) {
        mModel.likePost(globalCurrentUserId,globalPostId, new IGlobalListener<Boolean>() {
            @Override
            public void onComplete(Boolean object) {
                if(object){
                    listener.onComplete(true);
                    return;
                }
                listener.onComplete(false);
            }
        });
    }

    private void CommentOnPost(final IGlobalListener<Boolean> listener){
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setCommentDate(new Date());
        commentDTO.setCommentId(StringsGenerator.GenerateNewUUID());
        commentDTO.setPostId(globalPostId);
        Editable textEdit = edit_text_comment_post_page.getText();
        commentDTO.setText(textEdit != null ? textEdit.toString() : "");
        commentDTO.setUserId(globalCurrentUserId);

        mModel.commentOnPost(globalCurrentUserId, globalPostId, commentDTO, new IGlobalListener<Boolean>() {
            @Override
            public void onComplete(Boolean object) {
                if(object){
                    listener.onComplete(true);
                }
            }
        });
    }

    private void DeletePost() {
        mModel.deletePost(globalPostId, new IGlobalListener<Boolean>() {
            @Override
            public void onComplete(Boolean object) {
                if(object){
                    ((FrameLayout)findViewById(R.id.post_page_lower_part)).removeAllViews();
                    Toast.makeText(PostPageActivity.this,"Successfully Deleted!",Toast.LENGTH_LONG).show();
                    ChangeFragmentHandler.changeFragmentDisplay(PostPageActivity.this,null,R.id.post_page_lower_part,mDrawerLayout,getSupportFragmentManager(),R.id.home);
                }
                else{
                    Toast.makeText(PostPageActivity.this,"Couldn't Delete the post.. Please try again later.",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(mActionDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main_menu,menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    public class RVAdapterComments extends RecyclerView.Adapter<PostPageActivity.RVAdapterComments.CommentViewHolder> {

        Context mContext;
        List<CommentDTO> commentDTOList;

        public RVAdapterComments(List<CommentDTO> comments, Context context){
            commentDTOList = comments;
            mContext = context;
        }

        @NonNull
        @Override
        public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.comment_content, parent,false);
            CommentViewHolder viewHolder = new CommentViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final CommentViewHolder holder, int position) {
            final CommentDTO commentDTO = commentDTOList.get(position);

            modelManager.getUserById(commentDTO.getUserId(), new IGetUserListener() {
                @Override
                public void onComplete(UserDTO userDTO) throws MyException, NoSuchAlgorithmException {
                    if(userDTO != null){
                        holder.setIsRecyclable(false);
                        holder.setDetails(userDTO.getProfileImage(),userDTO.getFullName(),commentDTO.getText());
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            if(commentDTOList != null)
                return commentDTOList.size();
            return 0;
        }


        public class CommentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            View view;

            public CommentViewHolder(@NonNull View itemView) {
                super(itemView);
                view = itemView;
            }

            public void setDetails(String imagePathFromStorage, String fullName, String commentText){
                CircleImageView imageView = view.findViewById(R.id.comment_user_profile_iv);
                TextView userFullNameTextView = view.findViewById(R.id.comment_user_name);
                TextView commentTextView = view.findViewById(R.id.comment_text);

                Picasso.with(MyApplication.getContext()).load(imagePathFromStorage).into(imageView);
                userFullNameTextView.setText(fullName);
                commentTextView.setText(commentText);
            }

            @Override
            public void onClick(View v) {

            }
        }
    }
}































