package com.example.dewclaw_new.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.EExceptions;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Handlers.ErrorHandler;
import com.example.dewclaw_new.Helpers.EmailHelper;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.security.NoSuchAlgorithmException;

public class SignUpActivity extends AppCompatActivity {

    LinearLayout mLoginContainer;
    AnimationDrawable mAnimationDrawable;

    EditText username_et;
    EditText password_et;
    EditText email_et;
    EditText password_confirm_et;
    TextView go_to_login_btn;
    Button sign_up_btn;
    ProgressDialog mProgressDialog;

    ModelManager mModel;

    Object localDataObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mLoginContainer = findViewById(R.id.login_container);
        mAnimationDrawable = (AnimationDrawable) mLoginContainer.getBackground();

        mAnimationDrawable.setEnterFadeDuration(5000);
        mAnimationDrawable.setExitFadeDuration(2000);

        username_et = findViewById(R.id.user_name);
        password_et = findViewById(R.id.user_password);
        password_confirm_et = findViewById(R.id.user_password_confirm);
        email_et = findViewById(R.id.user_email);
        go_to_login_btn = findViewById(R.id.go_to_login);
        sign_up_btn = findViewById(R.id.sign_up_btn);


        mProgressDialog = new ProgressDialog(this);

        sign_up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    register();
                } catch (MyException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        });

        go_to_login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent loginIntent = new Intent(SignUpActivity.this,LoginActivity.class);
                startActivity(loginIntent);
            }
        });
    }

    private void register() throws MyException, NoSuchAlgorithmException {

        final String email = email_et.getText().toString();
        final String userName = username_et.getText().toString();
        final String password = password_et.getText().toString();
        String password_confirm = password_confirm_et.getText().toString();

        if(!password.equals(password_confirm)){
            ErrorHandler.setError(password_confirm_et,EExceptions.BadPasswordConfirmation);
            return;
        }
        if(!EmailHelper.ValidateEMail(email)){
            ErrorHandler.setError(email_et,EExceptions.EmailValidation);
            return;
        }
        if(password.isEmpty() || password_confirm.isEmpty()){
            ErrorHandler.setError(password_confirm_et,EExceptions.BadPasswordConfirmation);
            return;
        }

        mModel.getInstance(EModel.FireBase).createNewUser(new UserDTO(email, password, userName), new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if(task.isSuccessful()){
                    try {
                        mModel.getInstance(EModel.SqlLite).createNewUser(new UserDTO(email, password, userName),task1 -> {});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    startActivity(new Intent(SignUpActivity.this, UpdateDetailsActivity.class));
                }
                else {
                    ErrorHandler.setError(email_et,EExceptions.AlreadyExistUser);
                }
            }
        });


    }


    @Override
    protected void onPostResume() {
        super.onPostResume();

        if(mAnimationDrawable != null && !mAnimationDrawable.isRunning())
            mAnimationDrawable.start();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(mAnimationDrawable != null && mAnimationDrawable.isRunning())
            mAnimationDrawable.stop();

    }

    @Override
    protected void onStart() {
        super.onStart();

        if(((FireBaseModel)mModel.getInstance(EModel.FireBase)).getFireBaseAuthInstance().getCurrentUser() != null){
            startActivity(new Intent(this,MainActivity.class));
        }
    }
}
