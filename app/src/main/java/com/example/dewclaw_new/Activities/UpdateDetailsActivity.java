package com.example.dewclaw_new.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Fragments.MyDetailsFragment;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.R;

public class UpdateDetailsActivity extends AppCompatActivity {

    EditText email_tv;
    EditText first_name_tv;
    EditText last_name_tv;
    EditText age_tv;
    EditText dog_name_tv;
    EditText dog_age_tv;
    EditText dog_breed_tv;

    Button update_btn;

    ImageView detail_iv;

    FireBaseModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_details);

        first_name_tv = findViewById(R.id.register_details_first_name_tv);
        last_name_tv = findViewById(R.id.register_details_last_name_tv);
        age_tv = findViewById(R.id.register_details_age_tv);
        dog_name_tv = findViewById(R.id.register_details_dog_name_tv);
        dog_age_tv = findViewById(R.id.register_details_dog_age_tv);
        dog_breed_tv = findViewById(R.id.register_details_dog_breed_tv);

        update_btn = findViewById(R.id.register_details_update_btn);

        detail_iv = findViewById(R.id.register_details_iv);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserDetails();
            }
        });
    }

    private void updateUserDetails() {
        mModel = (FireBaseModel) ModelManager.getInstance(EModel.FireBase);

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(first_name_tv.getText().toString());
        userDTO.setLastName(last_name_tv.getText().toString());
        userDTO.setAge(age_tv.getText().toString());
        userDTO.setDogName(dog_name_tv.getText().toString());
        userDTO.setDogAge(dog_age_tv.getText().toString());
        userDTO.setDogBreed(dog_breed_tv.getText().toString());

        mModel.updateUserDetails(userDTO, new IGlobalListener<UserDTO>() {
            @Override
            public void onComplete(UserDTO object) {
                if(object != null){
                    startActivity(new Intent(UpdateDetailsActivity.this,MainActivity.class));
                }
            }
        });
    }
}
