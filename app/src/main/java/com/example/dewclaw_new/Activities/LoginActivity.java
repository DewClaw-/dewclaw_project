package com.example.dewclaw_new.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Exceptions.EExceptions;
import com.example.dewclaw_new.Exceptions.MyException;
import com.example.dewclaw_new.Handlers.ChangeFragmentHandler;
import com.example.dewclaw_new.Handlers.ErrorHandler;
import com.example.dewclaw_new.Helpers.EmailHelper;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.R;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class LoginActivity extends AppCompatActivity {

    LinearLayout mLoginContainer;
    AnimationDrawable mAnimationDrawable;

    EditText email_et;
    EditText password_et;
    TextView sign_up_btn;
    TextView forgot_pass_btn;
    Button login_btn;
    ProgressDialog mProgressDialog;

    DrawerLayout mDrawerLayout;


    List<AuthUI.IdpConfig> providers;

    ModelManager mModel;

    Object localDataObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginContainer = findViewById(R.id.login_container);
        mAnimationDrawable = (AnimationDrawable) mLoginContainer.getBackground();

        mAnimationDrawable.setEnterFadeDuration(5000);
        mAnimationDrawable.setExitFadeDuration(2000);

        mDrawerLayout = findViewById(R.id.main_drawer_layout);

        mModel = new ModelManager();

        email_et = findViewById(R.id.user_email);
        password_et = findViewById(R.id.user_password);
        sign_up_btn = findViewById(R.id.sign_up_btn);
        forgot_pass_btn = findViewById(R.id.forgot_pass_btn);
        login_btn = findViewById(R.id.login_btn);

        mProgressDialog = new ProgressDialog(this);

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogIn();
            }
        });

        sign_up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent signUpIntent = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(signUpIntent);
            }
        });

        forgot_pass_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ForgotPasswordProcess();
            }
        });

    }

    private void LogIn(){

        final String email = email_et.getText().toString();
        final String password = password_et.getText().toString();

        if(email.isEmpty()){
            ErrorHandler.setError(email_et,EExceptions.EmptyFieldMessage);
            return;
        }
        if(password.isEmpty()){
            ErrorHandler.setError(password_et,EExceptions.EmptyFieldMessage);
            return;
        }
        if(!EmailHelper.ValidateEMail(email)){
            ErrorHandler.setError(email_et,EExceptions.EmailValidation);
            return;
        }


        try {
            mModel.getInstance(EModel.FireBase).logUserIn(email,password, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful()){
                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                    }
                    else{
                        ErrorHandler.setError(email_et,EExceptions.WrongUsernameOrPassword);
                    }
                }
            });
        } catch (MyException e) {
            email_et.setError(e.getMessage());
        }
    }

    protected void onStart() {
        super.onStart();

        if(((FireBaseModel)mModel.getInstance(EModel.FireBase)).getFireBaseAuthInstance().getCurrentUser() != null){
            startActivity(new Intent(this,MainActivity.class));
        }
    }
}
