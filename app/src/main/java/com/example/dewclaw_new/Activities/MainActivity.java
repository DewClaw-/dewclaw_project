package com.example.dewclaw_new.Activities;


import android.content.Intent;
import androidx.annotation.NonNull;

import com.example.dewclaw_new.DTOs.ProfilePictureDTO;
import com.example.dewclaw_new.DTOs.UserDTO;
import com.example.dewclaw_new.Handlers.ChangeFragmentHandler;
import com.example.dewclaw_new.Handlers.UserDetailsHandler;
import com.example.dewclaw_new.Handlers.UserProfilePictureHandler;
import com.example.dewclaw_new.Models.FireBaseModel;
import com.example.dewclaw_new.Models.Intefraces.IGlobalListener;
import com.example.dewclaw_new.MyApplication;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import de.hdodenhof.circleimageview.CircleImageView;

import android.view.MenuItem;
import android.widget.TextView;

import com.example.dewclaw_new.Enums.EModel;
import com.example.dewclaw_new.Models.ModelManager;
import com.example.dewclaw_new.R;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;


public class MainActivity extends AppCompatActivity {

    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mActionDrawerToggle;
    NavigationView mNavigationView;
    ModelManager mModel;

    CircleImageView userImageView;

    TextView userFullNameView;
    TextView userEmailView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mDrawerLayout = findViewById(R.id.main_drawer_layout);
        mNavigationView = findViewById(R.id.main_nav_view);
        userImageView = findViewById(R.id.user_iv);


        mModel = new ModelManager();

        mModel.getInstance(EModel.SqlLite);


        mActionDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open,R.string.close);
        mDrawerLayout.addDrawerListener(mActionDrawerToggle);
        mActionDrawerToggle.syncState();
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setSupportActionBar((Toolbar)findViewById(R.id.my_toolbar));
        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_menu);

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                ChangeFragmentHandler.changeFragmentDisplay(MainActivity.this,null,R.id.main_fragment_content, mDrawerLayout, getSupportFragmentManager() ,menuItem.getItemId());
                return true;
            }
        });

        //default home fragment
//        ChangeFragmentHandler.changeFragmentDisplay(MainActivity.this,null,R.id.main_fragment_content, mDrawerLayout, getSupportFragmentManager() ,R.id.home);

        if(((FireBaseModel)mModel.getInstance(EModel.FireBase)).getFireBaseAuthInstance().getCurrentUser() != null)
            ChangeFragmentHandler.changeFragmentDisplay(MainActivity.this,null,R.id.main_fragment_content, mDrawerLayout, getSupportFragmentManager() ,R.id.home);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(mActionDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main_menu,menu);
//        return super.onCreateOptionsMenu(menu);
//    }


    @Override
    protected void onStart() {
        super.onStart();
        if(FirebaseAuth.getInstance().getCurrentUser() == null){
            startActivity(new Intent(this,LoginActivity.class));
        }
        else{

            UserProfilePictureHandler.getUserProfilePicture(((FireBaseModel)mModel.getInstance(EModel.FireBase)).getFireBaseAuthInstance().getCurrentUser().getUid()
                                                            ,new IGlobalListener<ProfilePictureDTO>() {
                @Override
                public void onComplete(ProfilePictureDTO object) {
                    if(object != null) {
                        userImageView = findViewById(R.id.user_iv);
                        if(object.NeedDefault){
                            userImageView.setImageResource(object.DEFAULT_PROFILE_PICTURE);
                        }
                        else{
                            Picasso.with(MyApplication.getContext()).load(object.ExternalPicture).into(userImageView);
                        }
                    }
                }
            });

            UserDetailsHandler.getUserDetails(new IGlobalListener<UserDTO>() {
                @Override
                public void onComplete(UserDTO object) {
                    if(object != null){
                        userFullNameView = findViewById(R.id.user_full_name);
                        userEmailView = findViewById(R.id.user_menu_email);
                        String fullName = generateFullName(object.getFirstName(),object.getLastName());
                        userFullNameView.setText(fullName);
                        userEmailView.setText(object.getEmail());
                    }
                }

                private String generateFullName(String firstName, String lastName) {
                    if(firstName == null)
                        return "";
                    if(firstName != null && lastName == null)
                        return firstName;
                    if(firstName != null && lastName != null)
                        return firstName+" "+lastName;
                    return "";
                }
            });

        }
    }



}






//                            String imagename = "default_profile_image";
//                            int res = getResources().getIdentifier(imagename, "drawable", getPackageName());
//                            userImageView = findViewById(R.id.user_iv);
//                            userImageView.setImageResource(res);